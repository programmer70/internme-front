import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Login from './Authorization/Login';
import PrivetRouting from './Authorization/PrivetRouting';
import Ragistration from './Authorization/Ragistrations';
import Singup from './Authorization/Singup';
import Feed from './Components/core/Feed';
import Home from './Components/core/Home';
import EmployerProfile from './Components/EmployerProfile';
import HotJob from './Components/HotJob';
// import Navigation from './core/Navigation';
import NotFound404 from './Components/NotFound404';
import Profile from './Components/Profile';
import MoreConnections from './Components/MorePeople';
import MyConnections from './Components/MyConnections';
import ManageMyNatwork from './Components/view/network/more_network/ManageMyNatwork';
import SinglePost from './Components/view/feed/postCRUD/SinglePost';


const AllRouter = () => (
    <div>
        {/* hare is navetions */}
        {/* <Navigation/> */}
        {/* belos is control all routs  */}
        <Switch>
            <Route exact path={['/', '/home']} component={Home} /> {/* multiple url for single page (pass as a list) */}
            <PrivetRouting exact path='/feed' component={Feed} />
            <Route exact path='/singup/:userType' component={Singup} />    {/* userType -> employer or student */}
            <Route exact path='/ragisteration/:userType/:id-:token' component={Ragistration} />
            <Route exact path='/login' render={(props) => <Ragistration {...props} />} component={Login} /> {/* pass props to specific components */}
            <Route exact path='/profile/:fname-:lname-:uid' component={Profile} /> {/* only candidate profile */}
            <Route exact path='/profile/:orgname-:uid' component={EmployerProfile} /> {/* only employer profile */}
            <PrivetRouting exact path='/morepeople' component={MoreConnections} />
            <PrivetRouting exact path='/connections/:userId' component={MyConnections} />
            {/* <PrivetRouting exact path='/post/:postId' component={SinglePost} /> this will show single post */}
            <Route exact path='/job' component={HotJob} />
            <Route component={NotFound404} /> {/* render not foud page if not matching any rout */}
        </Switch>
    </div>
)
export default AllRouter;