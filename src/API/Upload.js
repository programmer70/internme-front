import axios from "axios";
import { APIurl } from "../variables";
import { isAuthenticate } from "./Autho";


const wtToken = isAuthenticate().jwtoken;

export const fileUploadAPI = (urproOrhrproOrurpostOrhrpost, newOrupdate, imgOrvideoOrdoc, profilidOrpostId, profileTookeOrpostRole, filetype, profileorpost, data) => {
    console.log('token in api -> ', data);
    /**
     * @naseemkhan7021
     * @description
     * @param {
     * 1.   profilidOrpostId
     * 2.   profileTookeOrpostRole
     * 3.   filetype     
     *  }
     * 
     * Axios.post(`${uploadUrl}/${token1}-${token2}/${imgcovevideo}/:profileorpost`, formData, config)
     */
    let uploadUrl = `/${urproOrhrproOrurpostOrhrpost}/${newOrupdate}/${imgOrvideoOrdoc}/${profilidOrpostId}-${profileTookeOrpostRole}/${filetype}/${profileorpost}`;
    console.log('porp-> ', profileorpost)
    return axios({
        url: uploadUrl,
        baseURL: APIurl,
        method: 'POST',
        headers: {
            "content-type": "multipart/form-data" ,
            'Authorization': `Bearer ${wtToken}`
        },
        data: data
    })

        .then(responce => responce.data)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}


// export const updateUploadAPI = (urproOrhrproOrurpostOrhrpost,imgOrvideoOrdoc,profilidOrpostId) => {
//     let uploadUrl = `${APIurl}/${urproOrhrproOrurpostOrhrpost}/new/${imgOrvideoOrdoc}`;

// }