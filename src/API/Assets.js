/*
    @Description => this use to handel ---> follow ,unfollow,like,comment and more like media
    @rootapi => http://localhost:8080/api/assets
    @method => Post and get
    @access => Privet (token require)
*/

import axios from "axios"
import { APIurl } from "../variables"
import { isAuthenticate } from "./Autho";

export const FollowApiCall = (data, token) => {

    console.log('FollowApiCall data ', data);
    console.log('FollowApiCall token ', token);

    return axios({
        baseURL: APIurl,
        url: '/assets/follow',
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        },
        data: data
    }).then(responce => responce)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const unFollowApiCall = (data, token) => {

    // console.log('unFollowApiCall data ', data);
    // console.log('unFollowApiCall token ',token);
    return axios({
        baseURL: APIurl,
        url: '/assets/ufollow',
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        },
        data: data
    }).then(responce => responce)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) { // responce ---> response --> speling mistak
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}


export const likeandUnlike = (data, token) => {
    // console.log('unFollowApiCall data ', data);
    // console.log('unFollowApiCall token ',token);
    return axios({
        baseURL: APIurl,
        url: `/post/like/${data.postId}-${data.postRole}/${data.authuserId}-${data.authuserRole}`,
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        }
    }).then(responce => responce)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

export const addCommentsAPI = ({ postId, postRole, userDetailId, userRole, text }) => {
    let token = isAuthenticate() && isAuthenticate().jwtoken
    return axios({
        baseURL: APIurl,
        url: `/post/add_comment/${postId}-${postRole}/${userDetailId}-${userRole}`,
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        data: {
            text
        }
    }).then(response => response)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}