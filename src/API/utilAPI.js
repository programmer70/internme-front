/*
    @Description => this use to handel ---> give some imp data like (give delails id by user id)
    @rootapi => http://localhost:8080/api/give
    @method => Post and get
    @access => Privet and public both ()
*/

import axios from "axios"
import { APIurl } from "../variables"


export const giveDetailIdbyUserId = (userId) => {
    // console.log('log frm api useId ',userId);
    return axios({
        baseURL: APIurl,
        url: "/give/detailsidbyuserid",
        method:"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        data: userId
        
    }).then(responce => responce)
        .catch(error => {
            /* try to parsing error from backend */
            console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}