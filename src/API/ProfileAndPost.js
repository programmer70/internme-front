import { APIurl } from '../variables';
import { isAuthenticate } from './Autho';
import axios from 'axios';

// console.log(`${url}/singup`);
// const wtToken = isAuthenticate().jwtoken;
// console.log('tken -> ',wtToken);
export const creat_profileOrpost = (urproOrhrproOrurpostOrhrpost, profilidOrpostId, profileTokeOrRole, data) => {
    // console.log('data : -> ',data);
    let wtToken = isAuthenticate().jwtoken;
    let url = `/${urproOrhrproOrurpostOrhrpost}/new-info/${profilidOrpostId}-${profileTokeOrRole}`;
    // console.log('data -> ',data);
    // console.log('url -> ',url);
    return axios({
        url,
        baseURL: APIurl,
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${wtToken}`
        },
        data: data
    })
        .then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

export const update_profileOrpost = (urproOrhrproOrurpostOrhrpost, profilidOrpostId, profileTookeOrpostRole, data) => {
    // console.log('data : -> ',data);
    let wtToken = isAuthenticate().jwtoken;

    let url = `/${urproOrhrproOrurpostOrhrpost}/update-info/${profilidOrpostId}-${profileTookeOrpostRole}`;
    // console.log('jwtoken -> ',wtToken);
    // console.log('url -> ',url);
    return axios({
        url,
        baseURL: APIurl,
        method: 'PUT',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${wtToken}`
        },
        data: data
    })
        .then(response => response)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

// by id only 
export const showSingluser = (id) => {
    return axios({
        url: `/userProfile/user/${id}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}
// by id only 
export const showSinglemployer = (id) => {
    return axios({
        url: `/employerProfile/user/${id}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

// by uniq code and fname lname
export const showSingluserbyName = ({ fname, lname, uid }) => {
    // console.log('fname lname id ',fname ,' ',lname,' ',uid)
    return axios({
        url: `/userProfile/userbyname/${fname}-${lname}-${uid}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

// by uniq code and orgname
export const showSinglEmployerbyName = ({ orgname, uid }) => {
    // console.log('orgname id ',orgname,uid)
    return axios({
        url: `/employerProfile/userbyname/${orgname}-${uid}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}


export const checkUser = async (id, token) => {
    return await axios({
        url: `/userProfile/user-check/${id}-${token}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

export const checkEmployer = async (id, token) => {
    return await axios({
        url: `/employerProfile/user-check/${id}-${token}`,
        baseURL: APIurl,
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    }).then(response => response.data)
        .catch(error => {
            // console.log(error);
            return { error: error.response.data.error }
        });
}

