/**
 * this file will show all data, It will show all candidate, employer and post list
 * @naseemkhan7021 -- owner
 * @roll -- "It will show all candidate, employer and post list"
 * @visiblity -- privat (only login users)
 * @BaseUrl --- http://localhost:8080
 * @CandidateUrl --- @BaseUrl//api/userProfile/candidates/all
 * @EmployerUrl --- @BaseUrl//api/userProfile/candidates/all
*/

import { candidateProfile, employerProfile, postUrl } from "../variables";
import axios from "axios";
import { isAuthenticate } from "./Autho";


const token = isAuthenticate().jwtoken;

export const showAllCandidatesOrEmployer = (token, candiateOremployer) => {
    /**
     * this will show list of candidates and employer what we need based on @parameter --> candiateOremployer   
     */
    // console.log('baseurl  -> ',candidateProfile);
    return axios({
        baseURL: candiateOremployer === 'candidates' ? candidateProfile : employerProfile,
        url: `/${candiateOremployer}/all`,
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        }
    }).then(responce => responce.data)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
                // console.log('error -> ', errorjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const showAllconnections = (token, candiateOremployer, data) => {
    // console.log('data -> ',data);
    // console.log('token -> ',token);
    // console.log('CoE -> ', candiateOremployer);
    /**
     * this will show list of connections --> candiateOremployer[candidate,employer]
     */
    return axios({
        baseURL: candiateOremployer === 'employer' ? `${employerProfile}/employers` : `${candidateProfile}/candidates`,
        url: `/connections`,
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        },
        data: data
    }).then(responce => responce.data)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
                // console.log('error -> ', errorjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const showAllpost = () => {
    // console.log('tkoen -> ',token);
    /**
     * this function will show you all post
     */
    return axios({
        baseURL: postUrl,
        url: '/show/all',
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`
        }
    }).then(responce => responce.data)
        .catch(error => {
            // ** Here i am fetching the currect error
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
                console.log('error -> ', errorjson) 
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}