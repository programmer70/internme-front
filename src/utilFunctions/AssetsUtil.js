const { isAuthenticate } = require("../API/Autho");
const { giveDetailIdbyUserId } = require("../API/utilAPI");

let userDetailId;
export const checkFollowingOrNot = async data => {
    // giveDetailIdbyUserId() give authenticated user detail Id by authenticate user id  
    console.log('data check -> ', data);
    // console.log('data check -> ', data.map((item,i)=>{
    // item.onId && item.onId.find()
    // }).includes("60c7952766889a1c20ac7f77"));
    let userId = { userId: isAuthenticate() && isAuthenticate().tokend };

    let match;
    await giveDetailIdbyUserId(userId).then(responce => {
        // console.log('responce fll -> ', responce);
        if (responce.data) {
            userDetailId = responce.data && responce.data.userDetailId
            // console.log('data => ',data);
            match = data && data.map(item => {
                // console.log('item.onId ', item.onId);
                // console.log('userDetailId ', userDetailId);
                return item.onId && item.onId.includes(userDetailId)
            }).includes(true)
            // console.log('match ', match);
            // return match
        } if (responce.error) {
            console.log('error -> ', responce.error);
        }
    })
    // console.log('match ', match);
    return {match,userDetailId}
}


export const checkFollowingOrNotNewWay = async (data,id) => {
    // console.log('data -> ',data,' id -> ',id);
    let match = data && data.map(item => {
        return item.onId && item.onId.includes(id)
    }).includes(true)
    // console.log('match -> ',match);
    return {match}
}

export const checkLikedorNot = async (data,id) => { // data === liked list id is post id
    // console.log('data -> ',data,' id -> ',id);
    let match = data && data.map(item => {
        return item.posts && item.posts.includes(id)
    }).includes(true)
    // console.log('match -> ',match);
    return {match}
}