import { isAuthenticate } from "../API/Autho";
import { checkEmployer, checkUser, showSinglemployer, showSingluser } from "../API/ProfileAndPost";

export const userVerifyandGiveData =  async () => {
    // this will give logdin users data 
    // console.log('util fun');
    let candidateInfo ;
    let employerInfo ;
    let id = isAuthenticate() && isAuthenticate().tokend,
        token = isAuthenticate() && isAuthenticate().token,
        userType = isAuthenticate() && isAuthenticate().role;
    if (userType === 'student') {
        // console.log('usertype student');
        await checkUser(id, token).then( async (responce) => {
            // console.log('reponce for isAuth -> ',responce)
            // let candidateInfo;
            if (responce.error) {
                // console.log('erro -> ', responce.error);
                candidateInfo = { redirectHome: true, loading: false,error:true}
                // return condidateErrorInfo
            }
            else if  (responce.success || responce.detail) {
                // console.log('inof ',parsinfo );
                // console.log('respoce -> ', responce);
                // this.setState({ redirectHome: false, loading: false })
                await showSingluser(id).then(  responce => {
                    // console.log('singl user responce ', responce);

                    candidateInfo = { data: responce.user, imgpic: responce.user.profileImg.fileName,loading:false,error:false }
                    // candidateInfo = 'candidateInfo';
                    // return candidateInfo = candidateInfo;
                })
            }
        })
        return candidateInfo;
    }
    if (userType === 'employer') {
        // console.log('usertype employer');
        await checkEmployer(id, token).then( async (responce) => {
            // console.log('reponce for isAuth -> ',responce)
            if (responce.error) {
                // console.log('erro -> ', responce.error);
            //    const employerErrorInfo = { redirectHome: true, loading: false }
            employerInfo = { redirectHome: true, loading: false,error:true }
            }
            else if (responce.success || responce.detail) {
                // console.log('inof ',parsinfo );
                // console.log('respoce -> ', responce);
                // this.setState({ redirectHome: false, loading: false })
                await showSinglemployer(id).then(responce => {
                    // console.log('singl user responce ', responce);

                    // const employerNavInfo = { data: responce.user, imgpic: responce.user.orgLogo.fileName }
                    employerInfo = { data: responce.user, imgpic: responce.user.orgLogo.fileName,loading:false,error:false }
                })
                // return responce;
            }
        })
        return employerInfo;
    }
}


