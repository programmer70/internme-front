import React, { Component } from 'react';
import ManageMyNatwork from './view/network/more_network/ManageMyNatwork';
import '../utilities.css'
import CandidatesList from './view/network/more_network/CandidatesList';
import EmployerList from './view/network/more_network/EmployerList';
import { userVerifyandGiveData } from '../utilFunctions/GiveDatatoNavigation';
import Navigation from './core/Navigation';
import { isAuthenticate } from '../API/Autho';
import { TIMI_OUT } from '../variables';

class MoreConnections extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: '',
            message: '',
            success: false,
            loading: false,

            redirectHome: false,
            logdin: false,
            data: {},
            imgpic: '',
            connectionsLength: 0
        }

        document.title = `More People | InternMe`

    }

    // this messages come from child components 
    setMessage = message => {
        this.setState({ message })
        setTimeout(() => {
             this.setState({ message: '' })
        }, TIMI_OUT); // this value come form variable file, value is 3000 now
   }

    // if use have not any connection the bool is false otherwise it's tree --> this handle ManageMyNatwork commponent white areay
    AddconnectionsLength = (number) => {
        console.log('number -> ', number);
        this.setState({ connectionsLength: this.state.connectionsLength + number })
    }


    componentDidMount() {
        this.setState({ loading: true })
        // console.log('component did mount');
        if (isAuthenticate()) {
            userVerifyandGiveData().then(responce => {
                // console.log('responce --> ', responce);
                if (responce.error === true) {
                    // console.log('eror on verifydata');
                    this.setState({ loading: responce.loading, error: responce.error, logdin: false })
                }
                this.setState({ loading: responce.loading, data: responce.data, imgpic: responce.imgpic, logdin: true })
            }).catch(error => {
                console.log('error -> ', error);
            });
        }
        this.setState({ loading: false, logdin: false })
    }




    render() {
        const { loading, logdin, data, imgpic, message, connectionsLength } = this.state;
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <>
                    <header>
                        <Navigation fname={data.firstName} lname={data.lastName} unid={data.uniq} profileimg={imgpic} orgName={data.orgName} logdin={logdin} />
                    </header>
                    <div className='content-body-padding'>
                        {
                            (

                                message ? (
                                    <div className="alert allert-top-center-position alert-success alert-dismissible text-start fade show" role="alert">
                                        <strong>Success!</strong> {message}.
                                        {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                    </div>
                                ) : ''

                            )
                        }
                        <section className="container-lg">
                            <div className="row">

                                <div className="col-md-9">
                                    <div className="cardsContainer bg-white eachSection p-3">

                                        {/* show all candidates  */}
                                        <div id="candidates">

                                            <CandidatesList authUserDetails={{ detailId: data._id,connections: data.connections }} setmessage={this.setMessage} />

                                        </div>

                                        {/* show all employers  */}
                                        <div id="employers">
                                            <EmployerList authUserDetails={{ detailId: data._id,connections: data.connections }} setmessage={this.setMessage} />

                                        </div>


                                    </div>
                                </div>

                                <div className="col-md-3">
                                    <div className={`${connectionsLength !== 0 ? 'networkManagmentContainer  bg-white  eachSection  p-3' : ''} `}>
                                        <ManageMyNatwork authUserInfo={{ authDetailId: data._id, connections: data.connections }} setmessage={this.setMessage} AddconnectionsLength={this.AddconnectionsLength} />
                                    </div>
                                    {/* <ManageMyNatwork AddconnectionsLength={this.AddconnectionsLength} /> */}

                                </div>

                            </div>
                        </section>
                    </div>
                </>
            )
        );
    }
}

export default MoreConnections;
