import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticate } from '../../../API/Autho';
import { update_profileOrpost } from '../../../API/ProfileAndPost';
import '../../../utilities.css';

class Contect extends Component {
    constructor(params) {
        super(params)
        this.state = {
            social: { facebook: '', instagram: '', youtube: '', twitter: '', linkedin: '' },
            address: { state: '', country: '', city: '' },
            phone: '',
            telphone: '',


            error: '',
            successMassge: '',
            loading: false
        }
    }

    componentDidMount() {
        // console.log('props -> ', this.props.userContect);
        let object = this.props.userContect;
        let obj2 = this.state;
        // console.log('prit this.stat =-> ', obj2);

        // take the obj from parant class and set as child state
        for (const key in this.state) {
            obj2[key] = object[key]
        }
        // console.log('prit this.stat =-> ', obj2);
        this.setState(obj2)
    }

    // checke the validation 
    isValid = () => {
        const { phone, telphone } = this.state;
        this.setState({ error: '', loading: true })

        if (phone.length > 10 || phone.length < 10) {
            this.setState({ error: 'Phone must have 10 charecters only ', loading: false })
            return false
        }
        if (telphone.length > 15 || telphone.length < 8) {
            this.setState({ error: 'Telphone max langth have 15 charecters only ', loading: false })
            return false
        }
        return true
    }

    // handel the change 
    handaleValueChange = (name) => event => {
        this.setState({ loading: false, error: '' })
        if (name === 'phone' || name === 'telphone') {
            // console.log('phone telphone');
            this.setState({ [name]: event.target.value })

        }
        if (name === 'city' || name === 'state' || name === 'country') {
            // console.log('chenge address'); 
            // this method is very usefull for update nested obj in state 
            this.setState(prevState => ({
                ...prevState,
                address: {
                    ...prevState.address,
                    [name]: event.target.value
                }
            })
            )

        }
        if (name === 'facebook' || name === 'instagram' || name === 'youtube' || name === 'linkedin'|| name === 'twitter') {
            // console.log('chenge social');
            this.setState(prevState => ({
                ...prevState,
                social: {
                    ...prevState.social,
                    [name]: event.target.value
                }
            })
            )
        }
    }

    updateButtonClick = (e) => {
        const { social, address, phone, telphone } = this.state;
        e.preventDefault()
        let id = isAuthenticate().tokend
        let token = isAuthenticate().token
        this.setState({ error: '', loading: true })
        let data = {};
        if (this.isValid()) {

            data.phone = phone;
            data.telphone = telphone;
            data.address = address
            data.social = social
            // console.log('valid');
            update_profileOrpost('employerProfile', id, token, data).then(responce => {
                // console.log('responce -> ', responce)
                if (responce) {
                    this.setState({ successMassge: responce.massage, loading: false });
                    // window.location.reload()
                    this.props.setmassage(responce.massage)
                    document.getElementById('cnt-form').reset()
                    setTimeout(() => {
                        document.getElementById('contect-model-close').click()
                    }, 2000);

                }
                else this.setState({ error: responce.error, loading: false })

            })
        }

        // auto hide the all message and error in 3 secund (please call this fuction in class method )
        setTimeout(() => {
            this.setState({ successMassge: '', error: '' })
        }, 3000);
    }


    render() {
        const { successMassge, loading, error, phone, telphone, address, social } = this.state
        const { city, state, country } = address;
        return (
            <div className='bg-white pb-3 p-3'>
                <h1 className='secund-heading border-bottom text-capitalize fs-4'>Contect Organization {this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-pen" data-bs-toggle="modal" data-bs-target="#editContect"></i> : ''}</h1>
                <div className='ps-5 pr-3'>
                    {/* <p>THis is more content</p> */}
                    <p className='border-bottom py-2'><span className='fw-bold'>Phone</span><br /><span className='font-p9  ps-3'>{phone}</span></p>
                    <p className='border-bottom py-2'><span className='fw-bold'>Telphone</span><br /><span className='font-p9  ps-3'>{telphone}</span></p>

                    <p className='border-bottom py-2 text-capitalize'><span className='fw-bold'>Address</span><br /><span className='font-p9  ps-3'>{this.props.userContect.address ? address.city : ''} - {this.props.userContect.address && address.state}/ {this.props.userContect.address && address.country}</span></p>

                    <p className='border-bottom py-2'><span className='fw-bold'>Email</span><br /><span className='font-p9  ps-3'><a target="_blanck" href={`mailto:${this.props.userContect.email}`} className='d-flex font-p9  ps-3'>Send me mail</a></span></p>

                    {social.linkedin || social.youtube || social.facebook || social.instagram || social.twitter ? (
                        <p className='border-bottom py-2 pb-4'><span className='fw-bold'>Social</span><br /><span className='d-flex font-p9  ps-3'>
                        {
                            this.props.userContect.social && social.linkedin
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.linkedin.com/in/${social.linkedin}`}><i class="font-s1p3 fab fa-linkedin"></i></a></div>
                                ) : ''
                        }{
                            this.props.userContect.social && social.twitter
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://twitter.com/${social.twitter}`}><i class="font-s1p3 fab fa-twitter"></i></a></div>
                                ) : ''
                        }{
                            this.props.userContect.social && social.youtube
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.youtube.com/c/${social.youtube}`}><i className=" font-s1p3 fab fa-youtube"></i></a></div>
                                ) : ''
                        }{
                            this.props.userContect.social && social.facebook
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.facebook.com/profile.php?id=${social.facebook}`}><i className=" font-s1p3 fab fa-facebook-square"></i></a></div>
                                ) : ''
                        }{
                            this.props.userContect.social && social.instagram
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.instagram.com/${social.instagram}`}><i className=" font-s1p3 fab fa-instagram"></i></a></div>
                                ) : ''
                        }</span></p>
                    ): ''}
                    
                </div>

                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="editContect" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title" id="staticBackdropLabel">Edit Contect</h5>
                                <button type="button" className="btn-close" id="contect-model-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form className="row g-3 text-start" id="cnt-form">


                                    <div className="col-12">
                                        <label htmlFor="phone" className="form-label">Phone</label>
                                        <input value={phone} onChange={this.handaleValueChange('phone')} type="number" className="form-control" name="phone" id="phone" minLength="10" maxLength="10" placeholder="1234567890" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="phone" className="form-label">Telphone</label>
                                        <input value={telphone} onChange={this.handaleValueChange('telphone')} type="number" className="form-control" name="telphone" min="8" max="15" id="telphone" placeholder="1234567890" />
                                    </div>




                                    <p className='border-bottom fw-bold'>Address:</p>

                                    <div className="col-md-6">
                                        <label htmlFor="city" className="form-label">City</label>
                                        <input value={city} onChange={this.handaleValueChange('city')} type="text" placeholder="eg. Mumbai" className="form-control" name='city' id="city" />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="state" className="form-label">State</label>
                                        <input onChange={this.handaleValueChange('state')} type="text" value={address.state} placeholder="eg. Maharashtra" className="form-control" name='state' id="state" />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="country" className="form-label">Country</label>
                                        <input onChange={this.handaleValueChange('country')} required type="text" value={address.country} placeholder="eg. India" className="form-control" name='country' id="country" />
                                    </div>




                                    <p className='border-bottom fw-bold'>Social Network:</p>


                                    <div className="col-12">
                                        <label htmlFor="youtube" className="form-label">Youtube</label>
                                        <input onChange={this.handaleValueChange('youtube')} type="url" value={social.youtube} className="form-control" id="youtube" name="youtube" placeholder="eg. channel name" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="facebook" className="form-label">Facebook</label>
                                        <input onChange={this.handaleValueChange('facebook')} value={social.facebook} type="url" className="form-control" id="facebook" name="facebook" placeholder="eg. user id" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="instagram" className="form-label">Instagram</label>
                                        <input onChange={this.handaleValueChange('instagram')} value={social.instagram} type="url" className="form-control" id="instagram" name="instagram" placeholder="eg. user name" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="linkedin" className="form-label">linkedin</label>
                                        <input onChange={this.handaleValueChange('linkedin')} value={social.linkedin} type="url" className="form-control" id="linkedin" name="linkedin" placeholder="eg. user name" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="twitter" className="form-label">twitter</label>
                                        <input onChange={this.handaleValueChange('twitter')} value={social.twitter} type="url" className="form-control" id="twitter" name="twitter" placeholder="eg. user name/" />
                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.updateButtonClick} type="button" className="btn full-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contect;
