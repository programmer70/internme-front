import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { isAuthenticate } from '../../../API/Autho';
import { update_profileOrpost } from '../../../API/ProfileAndPost';
import { fileUploadAPI } from '../../../API/Upload';
import { checkFollowingOrNot } from '../../../utilFunctions/AssetsUtil';
import '../../../utilities.css';
import { employerImgurl, StudentImgurl } from '../../../variables';
import FollowAndUnfollow from '../../AssetsComponents/FollowAndUnfollow';

/**
 * come from props
 * profile img, cover img,
 * About , connections, follow or not
 * join at
 */
let userDetailId;

class Intro extends Component {
    constructor(params) {
        super(params)
        this.state = {
            avtar: '',
            avtarPath: '',
            coverimg: '',
            coverimgpath: '',
            docname: '',
            docpath: '',
            following: false,

            docTitle: '',
            aboutDoc: '',
            name: '',

            headline: '',
            join: '',

            error: '',
            loading: false,
            rediractwidhSuccess: false,
            successMassge: ''
        }
    }

    componentDidMount() {
        // update the state value use for in loops. userintro come from perant commponent
        let object = this.props.userintro;
        let obj2 = this.state;
        let connections = this.props.userintro && this.props.userintro.connections
        // console.log('prit this.stat =-> ', obj2);
        for (const key in this.state) {
            obj2[key] = object[key]
        }
        // console.log('prit this.stat =-> ', obj2);
        this.setState(obj2)
        if (isAuthenticate() && isAuthenticate().tokend && isAuthenticate().tokend !== this.props.uid) {
            checkFollowingOrNot(connections).then(responce => {
                // console.log('rsp ', responce);
                this.setState({ following: responce.match })
                userDetailId = responce.userDetailId
            });
        }
        // this.followUnfollowButtonClick();
    }

    // click on follow and unfollow button
    followUnfollowButtonClick = apiCall => {
        let token = isAuthenticate() && isAuthenticate().jwtoken;
        let data = {
            userId: userDetailId,
            userRole: isAuthenticate() && isAuthenticate().role,
            followUnFollowId: this.props.userDetailidAndRole && this.props.userDetailidAndRole.userDetailsId,
            followUnFollowRole: this.props.userDetailidAndRole && this.props.userDetailidAndRole.role
        };
        console.log('usdid ', userDetailId); 
        console.log('data => ', data);
        apiCall(data, token).then(responce => {
            console.log('responce -> ', responce);
            if (responce.data && responce.data.success) {
                this.setState({ following: !this.state.following })
                this.props.setmassage(responce.data.message)
            } else {
                console.log('error ', responce.error);
            }
        })

    }

    isValid = () => {
        const { name, headline } = this.state;
        this.setState({ error: '', loading: true })
        if (name.length === 0) {
            this.setState({ error: 'Name is required', loading: false })
            return false;
        }
        if (name.length <= 3) {
            this.setState({ error: 'Name must be 3 or more charecters', loading: false })
            return false;
        }
        if (headline.length === 0) {
            this.setState({ error: 'headline is required ', loading: false })
            return false;
        }
        if (headline.length > 100) {
            this.setState({ error: 'headline is must have under 100 charecters ', loading: false })
            return false
        }
        return true
    }

    handaleValueChange = name => event => {
        this.setState({ loading: false, error: '' })
        this.setState({ [name]: event.target.value })
    }

    handalPhotoClick = name => e => {
        if (name === 'orgLogo') {
            document.getElementById('ProfileimgUpload').click()
            // console.log('profile click ->',e.target.files );
        }
        else if (name === 'cover') {
            document.getElementById('coverimgUpload').click()
            // console.log('cover click ->',e.target.files);
        }
    }

    handleFileChange = name => photo => {
        this.setState({ error: '', loading: true })
        var formData = new FormData();
        let id = isAuthenticate().tokend
        let token = isAuthenticate().token

        formData.append(name, photo.target.files[0])
        // console.log('formData -> ', formData);
        console.log('file -> ', photo.target.files[0]);

        if (name === 'orgLogo' || name === 'cover') {

            fileUploadAPI('employerProfile', 'update', 'imgupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else {
                    console.log('data is file -> ', data);
                    if (name === 'orgLogo') {
                        this.setState({ avtar: data.fileName, avtarPath: data.filePath, successMassge: 'Image Uploaded successfully !!', error: '', loading: false });
                    } else {
                        this.setState({ coverimg: data.fileName, coverimgpath: data.filePath, successMassge: 'Image Uploaded successfully !!', error: '', loading: false });
                    }
                }
            })
        } else if (name === 'otherOrgVerificationDocument') {
            fileUploadAPI('employerProfile', 'update', 'docupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else this.setState({ docname: data.fileName, docpath: data.filePath, successMassge: 'Image Uploaded successfully !!', error: '', loading: false });
            })
        }



        setTimeout(() => {
            this.setState({ error: "", successMassge: '' })
        }, 6000);

    };

    updateButtonClick = (e) => {
        const { headline, coverimg, docTitle, aboutDoc, coverimgpath, avtar, avtarPath, docname, docpath, name } = this.state;
        e.preventDefault()
        let id = isAuthenticate().tokend
        let token = isAuthenticate().token
        let clearF = document.getElementById('int-form')
        this.setState({ error: '', loading: true })
        let data = {};
        if (this.isValid()) {
            data.orgLogo = {
                fileName: avtar,
                Path: avtarPath
            }
            data.orgCoverImag = {
                fileName: coverimg,
                Path: coverimgpath
            }
            data.otherDocument = {
                fileName: docname,
                Path: docpath,
                title: docTitle,
                somthingAbout: aboutDoc,
            }
            data.orgName = name;
            data.headline = headline;

            update_profileOrpost('employerProfile', id, token, data).then(responce => {
                // console.log('responce -> ', responce)
                if (responce) {
                    this.setState({ successMassge: responce.massage, loading: false });
                    // window.location.reload()
                    // this.props.match.params.fname = fname
                    // this.props.match.params.lname = lname
                    this.props.setmassage(responce.massage)
                    clearF.reset()
                    setTimeout(() => {
                        document.getElementById('intro-close').click()
                    }, 2000);
                }
                else this.setState({ error: responce.error, loading: false })

            })

            setTimeout(() => {
                this.setState({ error: '', successMassge: '' })
            }, 3000);
        }
    }

    render() {
        const { avtar, coverimg, name, headline, docTitle, aboutDoc, loading, following, error, rediractwidhSuccess, successMassge } = this.state;
        // if (rediractwidhSuccess) {
        //     return <Redirect to={`/profile/${fname}-${lname}-${this.props.unqid}`} />
        // }
        return (
            <div className='bg-white pb-3'>
                <div className="imgViwer">
                    <div className='mb-3'>
                        <div className='coverImg'>
                            {/* cover img  */}
                            <img src={`${employerImgurl}/profile/${coverimg}`}
                                onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultcover.jpg`} className='cover' id="coverImg" />
                            <div className='profile avtar-pos rounded-circle d-flex'>
                                {/* profile img */}
                                <img src={`${employerImgurl}/profile/${avtar}`}
                                    onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} id="profileImg" className='profileImg object-p-top' />

                                {/* <span onClick={this.handalPhotoClick('profile')} className='ProfilCamra'><i className="fas fa-camera"></i></span> */}
                                {/* </div> */}
                            </div>
                            {/* <span onClick={this.handalPhotoClick('cover')} className='coverImgCamra'><i className="fas fa-camera"></i></span> */}
                        </div>
                    </div>
                </div>
                <div className='top-intro'>
                    {/* hare is name and connection  */}
                    <h1 className='secund-heading text-capitalize fs-1 d-flex align-items-center justify-content-between'>{name}<Link to={{ pathname: `/connections/${this.props.uid}`, state: { userType: 'employer',userName:name } }} className='font-p9 text-capitalize'><span> {this.props.userintro && this.props.userintro.connections ? this.props.userintro.connections.length : ''} connections</span></Link></h1>
                    {/* <Link to='#'><span>connections {this.props.userintro && this.props.userintro.conconnections ? this.props.userintro.conconnections.length : ''}</span></Link> */}
                    {/* headline  */}
                    <p className='text-capitalize pr-3'>
                        <span className=''>{this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-pen" data-bs-toggle="modal" data-bs-target="#editIntro"></i> : ''}
                        </span><p className='f-115rm'>{headline}</p><span className="sm-txt">
                            {/* address */}
                            {
                                this.props.userintro && this.props.userintro.address && this.props.userintro.address.city && this.props.userintro.address.state && this.props.userintro.address.country ? ` ${this.props.userintro.address.city}, ${this.props.userintro.address.state}, ${this.props.userintro.address.country}` : `` 
                            }
                        </span>
                    </p>
                    {/* join and follow  */}
                    <p className='text-capitalize mt-3'><span>join on {new Date(this.props.userintro.join).toDateString()}</span>{
                        this.props.uid && isAuthenticate() && isAuthenticate().tokend !== this.props.uid ? (<div className="d-inline"> · <FollowAndUnfollow following={following} ButtonClick={this.followUnfollowButtonClick} /></div>) : ''
                    }</p>
                </div>

                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="editIntro" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title" id="staticBackdropLabel">Edit Intro</h5>
                                <button type="button" id="intro-close" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form  id="int-form" className="row g-3 text-start">
                                    {/* avtar and cover img areay  */}
                                    <div className="imgViwer">
                                        <div className=' overflow-hidden mb-3'>
                                            <div className='coverImg'>
                                                <img src={`${employerImgurl}/profile/${coverimg}`}
                                                    onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultcover.jpg`} className='cover' id="coverImg" />
                                                <div className='profile rounded-circle d-flex'>
                                                    {/* <div className='' > */}
                                                    <img src={`${employerImgurl}/profile/${avtar}`}
                                                        onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} id="profileImg" className='profileImg' />

                                                    <input type="file" hidden accept="image/*" onChange={this.handleFileChange('orgLogo')} name='orgLogo' id="ProfileimgUpload" />

                                                    <span onClick={this.handalPhotoClick('orgLogo')} className='ProfilCamra'><i className="fas fa-camera"></i></span>
                                                    {/* </div> */}
                                                </div>
                                                <input onChange={this.handleFileChange('cover')} name='cover' type="file" hidden accept="image/*" id="coverimgUpload" />
                                                <span onClick={this.handalPhotoClick('cover')} className='coverImgCamra'><i className="fas fa-camera"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    {/* personale info areay */}
                                    <p className='border-bottom fw-bold '>Personal Info:</p>


                                    <div className="col-6">
                                        <label htmlFor="userName" className="form-label required">Organization Name</label>
                                        <input required type="text" value={name} onChange={this.handaleValueChange('name')} className="form-control" name="userName" id="userName" placeholder="1234 Main St" />
                                        <small>(This name will Be shown publicly)</small>
                                    </div>

                                    {/* about areay */}
                                    <p className='border-bottom fw-bold'>More Details:</p>
                                    <small>(This verification is for the organization)</small>
                                    <p className='border-bottom fw-bold'>Upload verification Document:</p>
                                    <div className="col-12">
                                        <label htmlFor="docTitle" className="form-label">Document Title</label>
                                        <input type="text" value={docTitle} onChange={this.handaleValueChange('docTitle')} className="form-control" id="docTitle" name="docTitle" placeholder="eg. Text bill" />
                                    </div>
                                    <div className="input-group mb-3 col-12">
                                        {/* <label className="input-group-text" htmlFor="resume">Upload</label> */}
                                        <input onChange={this.handleFileChange('otherOrgVerificationDocument')} accept=".pdf, .doc, .docx, .odt" type="file" className="form-control" name='otherOrgVerificationDocument' id="resume" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="aboutDoc" className="form-label">About Document</label>
                                        <textarea value={aboutDoc} maxLength="200" onChange={this.handaleValueChange('aboutDoc')} type="text" className="form-control " id="aboutDoc" name="aboutDoc" placeholder="eg. Somthing about Document ...." rows="5" required />
                                    </div>

                                    <div className="col-12">
                                        <label htmlFor="headline" className="form-label required">Headline</label>
                                        <textarea value={headline} onChange={this.handaleValueChange('headline')} minLength="100" maxLength="200" type="text" className="form-control " id="headline" name="headline" placeholder="eg. World Is Biggest Company || Awarded Company In Asia ...." rows="5" required />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" id='clintro' className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.updateButtonClick} type="button" className="btn full-primary" >Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Intro;
