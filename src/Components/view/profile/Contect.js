import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticate } from '../../../API/Autho';
import { update_profileOrpost } from '../../../API/ProfileAndPost';
import '../../../utilities.css';

class Contect extends Component {
    constructor(params) {
        super(params)
        this.state = {
            phone: '',
            address: {},
            social: {},
            email: '',

            goBphone: '',
            goBcity: '',
            goBstate: '',
            goBcountry: '',

            goByoutube: '',
            goBgithub: '',
            goBinstagram: '',
            goBfacebook: '',
            goBtwitter:'',
            goBlinkedin:'',

            rediractwidhSuccess: false,
            error: '',
            successMassge: '',
            loading: false
        }
    }
    componentDidMount() {
        // console.log('props -> ', this.props.userContect);
        let object = this.props.userContect;
        let obj2 = this.state;
        // console.log('prit this.stat =-> ', obj2);
        for (const key in this.state) {
            obj2[key] = object[key]
        }
        // console.log('prit this.stat =-> ', obj2);
        this.setState(obj2)
        const { phone, address, social } = this.state;
        this.setState({
            goBphone: phone,
            goBcity: address && address.city,
            goBstate: address && address.state,
            goBcountry: address && address.country,
            goByoutube: social && social.youtube,
            goBgithub: social && social.github,
            goBinstagram: social && social.instagram,
            goBfacebook: social && social.facebook,
            goBtwitter: social && social.twitter,
            goBlinkedin: social && social.linkedin,
        })
    }

    isValid = () => {
        const { goBphone } = this.state;
        this.setState({ error: '', loading: true })

        if (goBphone.length > 10 || goBphone.length < 10) {
            this.setState({ error: 'Phone must have 10 charecters only ', loading: false })
            return false
        }
        return true
    }

    handaleValueChange = name => event => {
        this.setState({ loading: false, error: '' })
        this.setState({ [name]: event.target.value })
    }

    updateButtonClick = (e) => {
        const { goByoutube, goBcity, goBcountry, goBfacebook, goBgithub, goBinstagram, goBphone, goBstate } = this.state;
        e.preventDefault()
        let id = isAuthenticate().tokend
        let token = isAuthenticate().token
        this.setState({ error: '', loading: true })
        let data = {};
        if (this.isValid()) {

            data.phone = goBphone;
            data.address = { city: goBcity, state: goBstate, country: goBcountry }
            data.social = { youtube: goByoutube, facebook: goBfacebook, instagram: goBinstagram, github: goBgithub }
            // console.log('valid');
            update_profileOrpost('userprofile', id, token, data).then(responce => {
                // console.log('responce -> ', responce)
                if (responce) {
                    this.setState({ successMassge: responce.massage, loading: false });
                    // window.location.reload()
                    this.props.setmassage(responce.massage)
                    document.getElementById('cnt-form').reset()
                    setTimeout(() => {
                        document.getElementById('contect-model-close').click()
                    }, 2000);

                }
                else this.setState({ error: responce.error, loading: false })

            })
        }
        setTimeout(() => {
            this.setState({ successMassge: '', error: '' })
        }, 3000);
    }


    render() {
        const { successMassge, loading, error, goBcity, goBcountry, goBfacebook, goBgithub, goBinstagram, goBphone, goBstate, goByoutube,goBtwitter,goBlinkedin } = this.state
        return (
            <div className='bg-white pb-3 p-3'>
                <h1 className='secund-heading border-bottom text-capitalize fs-4'>ContectMe {this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-pen" data-bs-toggle="modal" data-bs-target="#editContect"></i> : ''}</h1>
                <div className='ps-5 pr-3'>
                    {/* <p>THis is more content</p> */}
                    <p className='border-bottom py-2'><span className='fw-bold'>Phone</span><br /><span className='font-p9  ps-3'>{goBphone}</span></p>

                    <p className='border-bottom py-2 text-capitalize'><span className='fw-bold'>Address</span><br /><span className='font-p9  ps-3'>{this.props.userContect.address ? goBcity : ''} - {this.props.userContect.address && goBstate}/ {this.props.userContect.address && goBcountry}</span></p>

                    <p className='border-bottom py-2'><span className='fw-bold'>Email</span><br /><span className='font-p9  ps-3'><a target="_blanck" href={`mailto:${this.props.userContect.email}`}>Send me mail</a></span></p>

                    <p className='border-bottom py-2 pb-4'><span className='fw-bold'>Social</span><br /><span className='d-flex font-p9  ps-3'>
                        { /* github */
                            this.props.userContect.social && goBgithub
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://github.com/${goBgithub}`}><i className=" font-s1p3 fab fa-github"></i></a></div>
                                ) : ''
                        }{/* youtube */
                            this.props.userContect.social && goByoutube
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.youtube.com/c/${goByoutube}`}><i className=" font-s1p3 fab fa-youtube"></i></a></div>
                                ) : ''
                        }{/* twitter */
                            this.props.userContect.social && goBtwitter
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://twitter.com/${goBtwitter}`}><i className="font-s1p3 fab fa-twitter"></i></a></div>
                                ) : ''
                        }{/* linkedIn */
                            this.props.userContect.social && goBlinkedin
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.linkedin.com/in/${goBlinkedin}`}><i className="font-s1p3 fab fa-linkedin"></i></a></div>
                                ) : ''
                        }{/* facebook */
                            this.props.userContect.social && goBfacebook
                                ? (
                                    <div className='p-3 py-2 border-end'><a className='' target="_blanck" href={`https://www.facebook.com/profile.php?id=${goBfacebook}`}><i className=" font-s1p3 fab fa-facebook-square"></i></a></div>
                                ) : ''
                        }{/* instagram */
                            this.props.userContect.social && goBinstagram
                                ? (
                                    <div className='p-3 py-2'><a className='' target="_blanck" href={`https://www.instagram.com/${goBinstagram}`}><i className=" font-s1p3 fab fa-instagram"></i></a></div>
                                ) : ''
                        }</span></p>
                </div>

                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="editContect" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title" id="staticBackdropLabel">Edit Contect</h5>
                                <button type="button" className="btn-close" id="contect-model-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form className="row g-3 text-start" id="cnt-form">


                                    <div className="col-12">
                                        <label htmlFor="phone" className="form-label">Phone</label>
                                        <input value={goBphone} onChange={this.handaleValueChange('goBphone')} type="number" className="form-control" name="phone" id="phone" placeholder="1234567890" />
                                    </div>




                                    <p className='border-bottom fw-bold'>Address:</p>

                                    <div className="col-md-6">
                                        <label htmlFor="city" className="form-label">City</label>
                                        <input value={goBcity} onChange={this.handaleValueChange('goBcity')} type="text" placeholder="eg. Mumbai" className="form-control" name='city' id="city" />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="state" className="form-label">State</label>
                                        <input onChange={this.handaleValueChange('goBstate')} type="text" value={goBstate} placeholder="eg. Maharashtra" className="form-control" name='state' id="state" />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="country" className="form-label">Country</label>
                                        <input onChange={this.handaleValueChange('goBcountry')} required type="text" value={goBcountry} placeholder="eg. India" className="form-control" name='country' id="country" />
                                    </div>




                                    <p className='border-bottom fw-bold'>Social Network:</p>

                                    <div className="col-12">
                                        <label htmlFor="github" className="form-label">GitHub</label>
                                        <input onChange={this.handaleValueChange('goBgithub')} type="url" value={goBgithub} className="form-control" id="github" name="github" placeholder="eg. https://github.com/" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="youtube" className="form-label">Youtube</label>
                                        <input onChange={this.handaleValueChange('goByoutube')} type="url" value={goByoutube} className="form-control" id="youtube" name="youtube" placeholder="eg. https://Youtube.com/" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="twitter" className="form-label">Twitter</label>
                                        <input onChange={this.handaleValueChange('goBtwitter')} type="url" value={goBtwitter} className="form-control" id="twitter" name="twitter" placeholder="eg. https://twitter.com/" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="linkedin" className="form-label">Linkedin</label>
                                        <input onChange={this.handaleValueChange('goBlinkedin')} type="url" value={goBlinkedin} className="form-control" id="linkedin" name="linkedin" placeholder="eg. https://linkedin.com/" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="facebook" className="form-label">Facebook</label>
                                        <input onChange={this.handaleValueChange('goBfacebook')} value={goBfacebook} type="url" className="form-control" id="facebook" name="facebook" placeholder="eg. https://Facebook.com/" />
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="instagram" className="form-label">Instagram</label>
                                        <input onChange={this.handaleValueChange('goBinstagram')} value={goBinstagram} type="url" className="form-control" id="instagram" name="instagram" placeholder="eg. https://Instagram.com/" />
                                    </div>


                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.updateButtonClick} type="button" className="btn full-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contect;
