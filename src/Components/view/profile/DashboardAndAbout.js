import React, { Component } from 'react';
import { isAuthenticate } from '../../../API/Autho';
import { update_profileOrpost } from '../../../API/ProfileAndPost';

class DashboardAndAbout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            about: '',

            message: '',
            error: false,
            seccess: false,
            loading: false
        }
    }

    handaleValueChange = name => event => {
        this.setState({ loading: false, error: '' })
        // console.log(event.target.value);
        this.setState({ [name]: event.target.value })
    }

    componentDidMount() {
        // this method can set all value then matches the this state come frome parent 
        let object = this.props.userinfo;
        let obj2 = this.state;
        // console.log('prit this.stat =-> ', obj2);
        for (const key in this.state) {
            obj2[key] = object[key]
        }
        // console.log('prit this.stat =-> ', obj2);
        this.setState(obj2)
    }

    updateButtonClick = (e) => {
        const { about } = this.state;
        e.preventDefault()
        let id = isAuthenticate().tokend
        let token = isAuthenticate().token
        this.setState({ error: '', loading: true })
        let data = {};

        data.about = about;
        // console.log('valid');
        update_profileOrpost('userprofile', id, token, data).then(responce => {
            // console.log('responce -> ', responce)
            if (responce.data) {
                this.setState({ successMassge: responce.data.massage, loading: false });
                // window.location.reload()
                this.props.setmassage(responce.data.massage)
                setTimeout(() => {
                    document.getElementById('about-model-close').click()
                }, 2000);

            }
            else this.setState({ error: responce.error, loading: false })

        })

        setTimeout(() => {
            this.setState({ successMassge: '', error: '' })
        }, 3000);
    }

    render() {
        const { about } = this.state;
        return (
            <>

                {
                    this.props.userinfo && isAuthenticate() && isAuthenticate().tokend === this.props.userinfo.userid ? (
                        <div className='bg-white br-1rem p-3 mb-3'>
                            <h1 className='secund-heading text-capitalize fs-4'>Deshboard</h1>
                            {
                                this.props.post ? (
                                    <div>Yes</div>
                                ) : (
                                    <div>
                                        No activity found
                                    </div>
                                )
                            }
                        </div>
                    ) : ''
                }
                <div className='bg-white br-1rem p-3'>
                    <h1 className='secund-heading text-capitalize fs-4'>About{this.props.userinfo && isAuthenticate() && isAuthenticate().tokend === this.props.userinfo.userid ? <i className="editIcon fas fa-pen" data-bs-toggle="modal" data-bs-target="#updateAbout"></i> : ''}</h1>
                    <div className="p-3">
                        <p className="font-p9 text-capitalize">
                            {about}
                        </p>
                    </div>
                    {/* model for update about */}
                    {/* <!-- Button trigger modal --> */}
                    {/* <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#updateAbout">
                        Launch static backdrop modal
                    </button> */}

                    {/* <!-- Modal --> */}
                    <div class="modal fade" id="updateAbout" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg  modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Edit about</h5>
                                    <button type="button" class="btn-close" id="about-model-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form className="row g-3 text-start" id="">
                                        <div className="col-12">
                                            <label htmlFor="phone" className="form-label">Summery</label>
                                            <textarea onChange={this.handaleValueChange('about')} name="about" value={about} id="about" rows="5" className="form-control" placeholder='Somthing about you'></textarea>
                                            {/* <input value={about}  type="number" className="form-control" name="phone" id="phone" placeholder="1234567890" /> */}
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button onClick={this.updateButtonClick} type="button" className="btn full-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default DashboardAndAbout;
