import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { isAuthenticate } from '../../../API/Autho';
import { showSinglEmployerbyName, showSingluserbyName } from '../../../API/ProfileAndPost';
import { employerImgurl, StudentImgurl } from '../../../variables';

export default class Myinfos extends Component {
     constructor(params) {
          super(params);
          this.state = {
               data: {
                    headline: '',
                    userName: '',
               },
               userType: isAuthenticate() && isAuthenticate().role,
               imgpic: '',
               profileCover: '',

               loading: true,
               errormessage: '',
               success: false
          }

     }

     componentDidMount() {
          // console.log('ust -> ',this.state.userType);

          let data;
          this.state.userType === 'student' ?
               data = {
                    fname: this.props.userInfo.fname,
                    lname: this.props.userInfo.lname,
                    uid: this.props.userInfo.uniq
               } : data = {
                    orgname: this.props.userInfo.orgName,
                    uid: this.props.userInfo.uniq
               }
          // console.log('data -> ', data);
          this.showUserData(data)  // this will employer or student data
     }

     // hare is fetching the data from back-end
     showUserData = data => {
          // call function by conditionaly (student or employer)
          this.state.userType === 'student' ?
               showSingluserbyName(data).then(responce => {
                    // console.log('responce pro -> ', responce);
                    if (responce.error) {
                         this.setState({ success: false, loading: false })
                    } else {
                         this.setState({ data: responce.user, profileCover: responce.user.profileCoverimg.fileName, imgpic: responce.user.profileImg.fileName, loading: false })
                    }
               }) :
               showSinglEmployerbyName(data).then(responce => {
                    // console.log('data pro -> ', data);
                    // console.log('responce pro -> ', responce);
                    if (responce.error) {
                         this.setState({ success: false, loading: false })
                    } else {
                         // console.log('responce -> ', responce.user);
                         this.setState({ data: responce.user, imgpic: responce.user.orgLogo.fileName, profileCover: responce.user.orgCoverImag.fileName, loading: false })
                         // document.title = `${responce.user.firstName} ${responce.user.lastName} | Profile`
                    }
               })
     }

     render() {
          const { loading, data, imgpic, profileCover, userType } = this.state
          return (
               loading ? (
                    <div className='loading-h-8rem  d-flex'>
                         <div className=' m-auto'>

                              <div className="spinner-border text-danger" role="status">
                                   <span className="visually-hidden">Loading...</span>
                              </div>
                         </div>
                    </div>
               ) : (
                    <div className="position-sticky" style={{ top: '5rem' }}>

                         <div className="bg-white   eachSection text-center">
                              {/* img cover name and headline */}
                              <div className="border-bottom p-3 position-sticky top-0">

                                   {/* cover img in dive */}
                                   <div style={{ backgroundImage: userType === 'student' ? `url(${StudentImgurl}/profile/${profileCover})` : `url(${employerImgurl}/profile/${profileCover})` }} className='profilecoverfeedstyle'></div>
                                   {/* img and name dive */}
                                   <Link to={userType == 'student' ? (`/profile/${data.firstName}-${data.lastName}-${data.uniq}`) : (`/profile/${data.orgName}-${data.uniq}`)} className='d-block'>
                                        <div className="">
                                             <div className="rounded-circle m-auto avtar-div centerY imgNormalS d-flex"><img src={userType == 'student' ? (`${StudentImgurl}/profile/${imgpic}`) : (`${employerImgurl}/profile/${imgpic}`)} width="64" height="64" className="img-fluid obj-fit-cover rounded-circle" onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} alt="avtar" /></div>
                                        </div>
                                        <div className="userName les20px text-capitalize fw-bold">{userType === 'student' ? data.userName : data.orgName}</div>
                                   </Link>
                                   <p className="card-text fs-6 fw-light mt-1">{data.headline}</p>
                              </div>
                              {/* total connections and post */}
                              <div className="d-flex flex-column my-3 fw-bold card-text fontOpacity1">
                                   <div className="hovgray">
                                        {/* link go to User (employer or candidate) connections list */}
                                        <Link to={userType === 'student' ? { pathname: `/connections/${data.user._id}`, state: { userType: data.user.role, userName: data.userName } } : { pathname: `/connections/${data.user._id}`, state: { userType: data.user.role, userName: data.orgName } }} className="d-block normalFontColor px-3 py-1">
                                             <div className="d-flex"><div className="text-start">connnections</div>
                                                  <div className="flex1 text-end"><span>{data.connections ? data.connections.length : 0}</span></div></div>

                                        </Link>
                                   </div>
                                   <div className="hovgray">
                                        <Link href="" className="d-block normalFontColor px-3 py-1">
                                             <div className="d-flex">
                                                  <div className="text-start">Post</div>
                                                  <div className="flex1 text-end"><span>56</span></div>
                                             </div>
                                        </Link>
                                   </div>
                              </div>

                              {/* favorite div */}
                              <Link className="normalFontColor"><div className="border-top hovgray py-2"><i className="fas fa-heart me-2"></i><div className="d-inline font-p9 fw-bolder">My Saved</div></div></Link>
                         </div>
                         <div className="bg-white  eachSection  p-3">
                              <h1 className="t">myinfo</h1>
                         </div>
                    </div>
               )
          )
     }
}
