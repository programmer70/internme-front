import React, { useEffect } from 'react'
import useIsInViewport from 'use-is-in-viewport'
import { employerVideourl } from '../../../../variables'

export default function VideoFunctionC(props) {
     const [isInViewport3, boxEl3] = useIsInViewport({
          // only when 50% of boxEl3 intersects with it's parent document viewport
          // mark it as visible in viewport
          threshold: 50
     });
     useEffect(() => {
          // console.log('view -> ', isInViewport3);
          if (isInViewport3 === true) {
               document.getElementsByClassName('play')[0].play()
          } else {
               let vs = document.getElementsByClassName('pause')
               for (var i = 0; i < vs.length; i++) {
                    vs[i].pause()
               }
          }
     })

     return (
          <div>
               <div className="w-100">
                    <video className={isInViewport3 === true ? 'play' : 'pause'} ref={boxEl3} poster="https://raw.githubusercontent.com/qaprosoft/carina/master/docs/img/video.png" src={`${employerVideourl}/posts/${props.video}`} preload="metadata" width="100%" controls>
                    </video>

               </div>
          </div>
     )
}
