import React from 'react'
import { Link } from 'react-router-dom'
import { employerDocurl, employerImgurl, employerVideourl, StudentDocurl, StudentImgurl, StudentVideourl } from '../../../../variables'
import LikeUnlikeButton from '../../../AssetsComponents/LikeUnlikeButton'
import VideoFunctionC from './VideoFunctionC'

export default function SinglePostModel(props) {
     return (
          <div className='h-100 d-flex'>
               <div className="m-auto modal-dialog modal-dialog-centered w-55 maxW100">
                    <div className="modal-content singpostModelH80VH">

                         <div className=" maxH100 modal-body">
                              <div className="d-flex h-100">
                                   <div className="d-flex align-items-center justify-content-center overflow-hidden col-7 blackBG">
                                        {/* this is video con  */}
                                        {
                                             props.postData.postedMediaFile && props.postData.postedMediaFile.photo && (
                                                  <div className="d-flex flex-wrap">
                                                       <img src={`${props.postData.role === 'student' ? StudentImgurl : employerImgurl}/posts/${props.postData.postedMediaFile.photo.fileName}`} alt="postimg" height='100%' width='100%' />

                                                  </div>
                                             )
                                        }
                                        {
                                             props.postData.postedMediaFile && props.postData.postedMediaFile.video && (
                                                  <div className="" >
                                                       <VideoFunctionC video={props.postData.postedMediaFile.video.fileName} />  {/* this becouse video auto play */}
                                                  </div>

                                             )
                                        }
                                        {
                                             props.postData.postedMediaFile && props.postData.postedMediaFile.doc && (<a target='_blank' href={`${props.postData.role === 'student' ? StudentDocurl : employerDocurl}/posts/${props.postData.postedMediaFile.doc.fileName}`}><span className="full-primary cursor-pointer"> {props.postData.postedMediaFile.doc.fileName.split('.')[0]} click to show </span></a>)
                                        }
                                   </div>
                                   <div className="col-5 ">
                                        <div className="modal-header position-relative d-block border-0">
                                             {/* <h5 className="modal-title" id="staticBackdropLabel">{props.postData.name}</h5> */}

                                             <div className="d-flex align-items-center">
                                                  {/* img div  */}
                                                  <div className="me-3 imgSmallS overflow-hidden rounded-circle">
                                                       {props.postData.role === 'student' ?
                                                            (
                                                                 <a href={`/profile/${props.postData.postedBy.fname}-${props.postData.postedBy.lname}-${props.postData.postedBy.uniq}`} ><div className="me-3 imgSmallS overflow-hidden rounded-circle">
                                                                      <img src={`${StudentImgurl}/profile/${props.postData.postedBy.profileImg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                                 </div></a>
                                                            ) : (
                                                                 <a href={`/profile/${props.postData.postedBy.orgName}-${props.postData.postedBy.uniq}`} ><div className="me-3 imgSmallS overflow-hidden rounded-circle">
                                                                      <img src={`${employerImgurl}/profile/${props.postData.postedBy.orgLogo.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                                 </div></a>
                                                            )
                                                       }
                                                  </div>
                                                  {/* name and headline div  */}
                                                  <div className="flex1">
                                                       <div className="overflow-hidden lineHight1">
                                                            <h5 className="f-14px text-capitalize ">
                                                                 <a href={props.postData.role === 'student' ? `/profile/${props.postData.postedBy.fname}-${props.postData.postedBy.lname}-${props.postData.postedBy.uniq}` : `/profile/${props.postData.postedBy.orgName}-${props.postData.postedBy.uniq}`} className="userName" >
                                                                      {props.postData.postedBy.orgName ? props.postData.postedBy.orgName : props.postData.postedBy.userName}
                                                                 </a> •
                                                                 <span className='sm-txt'>  {new Date(props.postData.updatedAt).toDateString()}
                                                                 </span>
                                                            </h5>
                                                            <a href={props.postData.role === 'student' ? `/profile/${props.postData.postedBy.fname}-${props.postData.postedBy.lname}-${props.postData.postedBy.uniq}` : `/profile/${props.postData.postedBy.orgName}-${props.postData.postedBy.uniq}`} className='color-unset fsXS' ><p className="text-capitalize"> {props.postData.postedBy.headline.slice(0, 49)}{props.postData.postedBy.headline.length > 50 ? ('...') : ('')}</p>
                                                                 <span className="fw-light fsXXS card-text">{props.postData.postedBy.address.city} • {props.postData.postedBy.address.state} • {props.postData.postedBy.address.country}</span></a>
                                                       </div>
                                                  </div>
                                                  {/* connect div if not connected  */}
                                                  {props.postData.postedBy._id === props.authD_id ? <i className="editIcon fas fa-pen" ></i> : (
                                                       props.postData.postedBy.connections.some(e => e.onId === props.authD_id) || props.followingOrNot ? '' : (<div className="">
                                                            <Link className="fw-bolder secund-heading p-1 primaryBGhover rounded" onClick={() => props.followClick(props.postData.postedBy._id, props.postData.role, props.postData.postedBy.userName || props.postData.postedBy.orgName)}> {/* <--- hare is conditionaly argument */}
                                                                 <span>
                                                                      &#10011; Follow
                                                                 </span>
                                                            </Link>
                                                       </div>))
                                                  }
                                                  {/* <div className="">
                                                       <Link className="fw-bolder secund-heading p-1 primaryBGhover rounded">
                                                            <span>
                                                                 &#10011; connect
                                                            </span>
                                                       </Link>

                                                  </div> */}
                                             </div>
                                             <div className="mx-3 py-2 mvh-50 overflowY-scroll hideScroll">
                                                  <p className=''>{props.postData.description} </p>
                                             </div>
                                             {/* like comment and share div (assat div ) */}
                                             <div className="mx-3">
                                                  {/* Social Detials */}
                                                  <div className="border-bottom py-2">
                                                       <span className="fsXS fontOpacity1 secund-heading">
                                                            <Link className="normalFontColor">likes <span className={`likeLenghth-${props.postData._id}`}> {props.postData.likes.length}</span></Link> <Link className="normalFontColor float-end">{props.postData.comments.length} comments</Link>
                                                       </span>
                                                  </div>
                                                  {/* Social activity  */}
                                                  <div className="fontOpacity1 my-2">
                                                       <LikeUnlikeButton postLikes={props.postData.likes} authLikedPost={props.authLikedPost} authInfo={{ detailsId: props.authD_id }} postIdandRole={{ role: props.postData.role, detailsId: props.postData._id }} />
                                                       <span className="px-2 hovgray postIconSize rounded cursor-pointer"><span className="me-2"><i className="fw200 fas fa-comment"></i></span>comment</span>
                                                       <span className="px-2 hovgray postIconSize rounded cursor-pointer"><span className="me-2"><i className=" fas fa-share"></i></span>share</span>
                                                  </div>
                                             </div>
                                             <button type="button" className="position-absolute top-0 end-0 btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>

                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     )
}
