import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticate } from '../../../../API/Autho';
import { creat_profileOrpost } from '../../../../API/ProfileAndPost';
import { fileUploadAPI } from '../../../../API/Upload';
import { employerImgurl, StudentImgurl } from '../../../../variables';


export default class Createpost extends Component {
     constructor(props) {
          super(props);
          this.state = {
               userType: isAuthenticate() && isAuthenticate().role,
               mediaType: '', // this for show or hide another accets
               mediaSelected: false, // auto disabled another media icon

               textarea: '',
               visible: 'AnyOne',
               commentOrnot: "AnyOne",
               postType: "NormalPost",
               image: '',
               video: '',
               doc: '',

               message: "",
               loading: false,
               error: ''
          }
     }

     // set medai not uploading 
     handChangeOnMediaUpload = name => e => {
          // console.log('handfile -> ', e);
          this.setState({ loading: true })
          switch (name) {
               case 'photo':
                    // console.log('img');
                    const image = e.target.files[0];
                    if (image === "" || image === undefined) {
                         console.error('img is only -> ', typeof (image));
                         this.setState({ error: `image is only - ${typeof (image)}` })
                    }
                    // console.log("img -> ", image);
                    this.setState({ image, mediaSelected: true, loading: false })
                    break;

               case 'video':
                    // console.log('video');
                    const video = e.target.files[0];
                    if (video === "" || video === undefined) {
                         console.error('img is only -> ', typeof (video));
                         this.setState({ error: `video is only - ${typeof (video)}` })
                    }
                    // console.log("v -> ", video);
                    this.setState({ video, mediaSelected: true, loading: false })
                    break;
               case 'doc':
                    // console.log('doc');
                    const doc = e.target.files[0];
                    if (doc === "" || doc === undefined) {
                         console.error('img is only -> ', typeof (doc));
                         this.setState({ error: `doc is only - ${typeof (doc)}` })
                    }
                    // console.log("img -> ",doc);
                    this.setState({ doc, mediaSelected: true, loading: false })
                    break;
          }


     }

     // click on main feed icons 
     handleMediaIconClick = name => e => {
          document.getElementById(name).click()
          switch (name) {
               case 'photo':
                    this.setState({ mediaType: name })
                    break;
               case 'video':
                    this.setState({ mediaType: name })
                    break;
               case 'doc':
                    this.setState({ mediaType: name })
                    break;


          }
     }
     // reset all the post details when click on X  
     resetPost = () => {
          // console.log('pr');
          if (this.state.textarea || this.state.mediaType) {
               switch (confirm('This will clear your written post, do you realy want to close it ???')) {
                    case true:
                         this.setState({ textarea: "", image: "", video: "", doc: "", mediaType: "", mediaSelected: false, error: "" })
                         break;
               }
          }


     }
     // reset only the media file 
     resetPostMedia = () => {
          this.setState({ mediaType: "", mediaSelected: false, image: "", video: "", doc: "", error: "" })
     }
     // this will send the data to backend ( call api form here)
     handleFormSubmint = (e) => {
          e.preventDefault();
          const { mediaType, image, video, doc, textarea, visible, commentOrnot, postType } = this.state
          this.setState({ loading: true, error: '' })
          const id = isAuthenticate() && isAuthenticate().tokend;
          const token = isAuthenticate() && isAuthenticate().token;
          var media;
          let data;
          let mediaUpload;
          const formdata = new FormData();
          switch (mediaType) {
               case 'photo':
                    mediaUpload = 'imgUpload';
                    data = image;
                    break;
               case 'video':
                    mediaUpload = 'videoUpload';
                    data = video;
                    break;
               case 'doc':
                    mediaUpload = 'docUpload';
                    data = doc;
                    break;

               default:
                    break;
          }
          document.getElementById(mediaType)
          // console.log('data -> ', data, ' type ', mediaType);
          formdata.append(mediaType, data)

          // upload media first 
          fileUploadAPI('post', 'new', mediaUpload, id, token, mediaType, 'posts', formdata).then(responce => {
               // console.log('file data-> ', responce);
               media = {
                    [mediaType]: {
                         fileName: responce.fileName,
                         filePath: responce.filePath
                    }
               }
          }).then(() => {
               // console.log('medai ', media);
               const mainData = {  // this obj will go backend
                    description: textarea, visible, commentOrnot, postType
                    , postedMediaFile: media
               }
               // console.log('data -> ', mainData);
               this.CreatPost(mainData)
          })
     }
     CreatPost = (mainData) => {
          const { detailId } = this.props.userinfo
          const role = isAuthenticate() && isAuthenticate().role
          // call api
          creat_profileOrpost('post', detailId, role, mainData).then(responce => {
               // console.log('responce -> ', responce)
               if (responce.success) {
                    this.setState({ message: responce.message, error: '', loading: false });
                    setTimeout(() => {
                         // this.resetPost()
                         document.getElementById('postModelDismiss').click()
                         this.props.setAllert(this.state.message, 'success')
                    }, 1000);
               }
               else {
                    this.setState({ error: responce.error, loading: false });
                    setTimeout(() => {
                         this.setState({ error: "" })
                    }, 2000);
               }
          })
     }



     render() {
          const { userType, textarea, image, video, doc, mediaType, mediaSelected, visible, commentOrnot, postType, loading, error } = this.state;
          return (
               <div>
                    <div className="bg-white  eachSection  p-3">
                         <div className="NewPostAreay">
                              {/* my input and img */}
                              <div className="inputandimg d-flex align-items-center">
                                   {isAuthenticate() && isAuthenticate().role === 'student' ?
                                        (
                                             <Link to={`/profile/${this.props.userinfo.fname}-${this.props.userinfo.lname}-${this.props.userinfo.uniq}`} ><div className="me-3 imgSmallS avtar-div overflow-hidden rounded-circle">
                                                  <img src={`${StudentImgurl}/profile/${this.props.userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                             </div></Link>
                                        ) : (
                                             <Link to={`/profile/${this.props.userinfo.orgName}-${this.props.userinfo.uniq}`} ><div className="me-3 imgSmallS avtar-div overflow-hidden rounded-circle">
                                                  <img src={`${employerImgurl}/profile/${this.props.userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                             </div></Link>
                                        )
                                   }

                                   {/* <!-- Button trigger modal --> */}
                                   <button autoFocus data-bs-toggle="modal" data-bs-target="#creatPostModel" className="font-p9 flex1 w-100 h-100 postinput hovgray cursor-pointer text-start" title="click hare to type post">
                                        <span className="fontOpacity1 fw-bold ">Start your post</span>
                                   </button>

                              </div>
                              {/* media file icons */}
                              <div className="medaiDiv d-flex justify-content-around mt-3">
                                   <span onClick={this.handleMediaIconClick("photo")} data-bs-toggle="modal" data-bs-target="#creatPostModel" className="cursor-pointer fontOpacity1"><div className="d-flex align-items-center postIconSize rounded hovgray"><i id="photoiconColor" className="fs-4 far fa-image"></i><span className="ms-3 fsXS secundheadingStyle fw-bolder">Photo</span></div></span>

                                   <span onClick={this.handleMediaIconClick("video")} data-bs-toggle="modal" data-bs-target="#creatPostModel" className="cursor-pointer fontOpacity1"><div className="d-flex align-items-center postIconSize rounded hovgray"><i id="videoiconColor" className="fs-4 fas fa-play-circle"></i><span className="ms-3 fsXS secundheadingStyle fw-bolder">Video</span></div></span>

                                   <span onClick={this.handleMediaIconClick("doc")} data-bs-toggle="modal" data-bs-target="#creatPostModel" className="cursor-pointer fontOpacity1"><div className="d-flex align-items-center postIconSize rounded hovgray"><i id="DociconColor" className="fs-4 fas fa-file-alt"></i><span className="ms-3 fsXS secundheadingStyle fw-bolder">Document</span></div></span>

                                   <span className="cursor-pointer fontOpacity1"><div className="d-flex align-items-center postIconSize rounded hovgray"><i id="eventiconColor" className="fs-4 fas fa-calendar-day"></i><span className="ms-3 fsXS secundheadingStyle fw-bolder">Event</span></div></span>
                              </div>
                         </div>
                    </div>

                    {/* hare is model for creating new post */}
                    {/* <!-- Modal --> */}
                    <div className="modal fade" id="creatPostModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="creatPostModelLabel" aria-hidden="true">
                         <div className="modal-dialog">
                              <div className="modal-content">

                                   <div className="modal-header">
                                        <h5 className="modal-title first-heading" id="creatPostModelLabel">Creat New Post</h5>
                                        <button onClick={this.resetPost} id="postModelDismiss" type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                   </div>
                                   <div className="modal-body">
                                        {
                                             loading ? (
                                                  <div className="spinner-border position-sticky d-block m-auto top-0 text-danger" role="status">
                                                       <span className="visually-hidden">Loading...</span>
                                                  </div>
                                             ) : ''
                                        }
                                        {
                                             error ? (
                                                  <div className="alert alert-danger position-sticky top-0 text-start fade show" role="alert">
                                                       <strong>Error!</strong> {error}.
                                                       {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                                  </div>
                                             ) : ''

                                        }

                                        <div className="userAvtar d-flex align-items-center mb-1">
                                             {isAuthenticate() && isAuthenticate().role === 'student' ?
                                                  (
                                                       <div className=" imgSmallS overflow-hidden rounded-circle">
                                                            <img src={`${StudentImgurl}/profile/${this.props.userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                       </div>

                                                  ) : (
                                                       <div className=" imgSmallS overflow-hidden rounded-circle">
                                                            <img src={`${employerImgurl}/profile/${this.props.userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                       </div>
                                                  )
                                             }
                                             <div className=" ms-1">
                                                  <div className="text-capitalize fw-bold">{userType === 'student' ? this.props.userinfo.userName : this.props.userinfo.orgName}</div>
                                                  {/* //Normal Post, internShip,intermidiat,associate,professinal or export */}
                                                  <div className="w-100 postPrivacyBtn fontOpacity1  btnRedias10 hovgray cursor-pointer"><i className="fas fa-comment"></i><select onChange={e => this.setState({ postType: e.target.value })} title="privacy" className="  ms-1 text-center font-p9" name="postType" id="postType">
                                                       <option value="NormalPost">NormalPost</option>
                                                       <option value="internShip">internShip</option>
                                                       <option value="intermidiat">intermidiat</option>
                                                       <option value="associate">associate</option>
                                                       <option value="professional">professional</option>
                                                       <option value="expart">expart</option>
                                                  </select></div>
                                             </div>

                                        </div>
                                        <div className="taxtarea">
                                             <textarea value={textarea} onChange={(e) => this.setState({ textarea: e.target.value.trimStart() })} className='w-100 border-0' name="postcontent" id="postcontent" placeholder="Write what are you thinking"></textarea>
                                             {/* <form onSubmit={this.handleFormSubmint} > */}
                                             <div className="handleupload">
                                                  <input onChange={this.handChangeOnMediaUpload("photo")} name="photo" id="photo" type="file" accept="image/*" hidden />
                                                  <input onChange={this.handChangeOnMediaUpload("video")} name="video" id="video" type="file" accept="video/*,video/mp4,video/avi,video/webm,video/x-ms-wmv,video/x-flv,video/mpeg,video/quicktime,video/x-m4v" hidden />
                                                  <input onChange={this.handChangeOnMediaUpload("doc")} name="doc" id="doc" type="file" accept=".doc,.docx,.pdf,.ppt,.pptx" hidden />

                                                  <div className="mediaLable text-center">
                                                       {
                                                            mediaType ? <label className="a-primary cursor-pointer" htmlFor={`${mediaType}`}>Click to Upload {mediaType}</label> : ''}
                                                  </div>
                                             </div>
                                             {/* </form> */}

                                        </div>
                                        <div className="MediaShowcontainer position-relative">

                                             {
                                                  /* CardDismiss icon */
                                                  image || video || doc ? <button onClick={this.resetPostMedia} className="cardDismissBtn z555 rounded-circle p-2">
                                                       <i className="cardDismiss fas fa-times"></i>
                                                  </button> : ''
                                             }
                                             {
                                                  image && <img src={`${URL.createObjectURL(image)}`} alt="imgOinput" className="img-fluid" />
                                             }
                                             {
                                                  video && <video controls preload="metadata" width="100%" src={`${URL.createObjectURL(video)}`}></video>
                                             }
                                             {
                                                  doc && <Link to={`${URL.createObjectURL(doc)}`}>{doc.name.split('.')[0]} click to show</Link>
                                             }

                                        </div>
                                   </div>
                                   <div className="modelAction">
                                        <div className="d-flex flex1 border-end fontOpacity1">
                                             {/* handel media  */}
                                             <button type='button' disabled={!mediaSelected ? false : true} onClick={this.handleMediaIconClick('photo')} title="Add Photo" className="postMediaIcon fontOpacity1 cursor-pointer p-1 rounded-circle hovgray"><i className="fs-4 far fa-image"></i></button>
                                             <button type='button' disabled={!mediaSelected ? false : true} onClick={this.handleMediaIconClick('video')} title="Add Video" className="postMediaIcon fontOpacity1 cursor-pointer p-1 rounded-circle hovgray"><i className="fs-4 fas fa-play-circle"></i></button>
                                             <button type='button' disabled={!mediaSelected ? false : true} onClick={this.handleMediaIconClick('doc')} title="Add Document" className="postMediaIcon fontOpacity1 cursor-pointer p-1 rounded-circle hovgray"><i className="fs-4 fas fa-file-alt"></i></button>
                                        </div>
                                        <div className="w-20 postPrivacyBtn fontOpacity1  btnRedias10 hovgray cursor-pointer"><i className="fas fa-comment"></i><select onChange={e => this.setState({ commentOrnot: e.target.value })} title="privacy" className="  ms-1 text-center font-p9" name="whoComment" id="whoComment">
                                             <option value="AnyOne">AnyOne</option>
                                             <option value="OnlyConnection">OnlyConnection</option>
                                             <option value="noOne">noOne</option>
                                        </select></div>
                                        <div className="w-20 postPrivacyBtn fontOpacity1  btnRedias10 hovgray cursor-pointer"><i className="fas fa-eye"></i><select onChange={e => this.setState({ visible: e.target.value })} title="privacy" className="  ms-1 text-center font-p9" name="whoSee" id="whosee">
                                             <option value="AnyOne">AnyOne</option>
                                             <option value="OnlyConnection">OnlyConnection</option>
                                             <option value="noOne">noOne</option>
                                        </select></div>
                                        <button onClick={this.handleFormSubmint} type="button" disabled={!textarea ? true : false} className="btn btn-primary btnRedias10 postbtn">Post</button>
                                   </div>

                              </div>
                         </div>
                    </div>
               </div >
          )
     }
}
