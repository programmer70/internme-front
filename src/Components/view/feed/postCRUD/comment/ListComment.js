import React, { useState } from 'react';
import { addCommentsAPI } from '../../../../../API/Assets';
import { isAuthenticate } from '../../../../../API/Autho'
import { employerImgurl, StudentImgurl } from '../../../../../variables'

export default function ListComment({ setAllert, postId, postRole, userinfo }) {
     const [comment, setComment] = useState('');

     const addComment = () => {
          let data = {
               postId,
               postRole,
               userDetailId: userinfo.detailId,
               userRole: isAuthenticate() && isAuthenticate().role,
               text: comment
          };
          addCommentsAPI(data).then(response => {
               console.log('response -> ', response)
               if (response.data && response.data.success) {
                    setAllert(response.data.message, 'success');
                    setComment('');
               } else {
                    setAllert(response.error, 'error');
               }

          });
     }

     return (
          <div className="row justify-content-center my-3 d-none" id={`comment-${postId}`}>
               <div className="col-2">

                    <div className="m-auto imgSmallS  overflow-hidden rounded-circle">
                         {isAuthenticate() && isAuthenticate().role === 'student' ?
                              (
                                   <div className="me-3 imgSmallS  overflow-hidden rounded-circle">
                                        <img src={`${StudentImgurl}/profile/${userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                   </div>
                              ) : (
                                   <div className="me-3 imgSmallS  overflow-hidden rounded-circle">
                                        <img src={`${employerImgurl}/profile/${userinfo.profileimg}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                   </div>
                              )
                         }
                    </div>
               </div>
               <div className="col-10">
                    {/* <form action=""> */}
                    <textarea placeholder='add a comment' onChange={(e) => setComment(e.target.value.trim())} type="text" rows="1" className='font-p9 flex1 w-100 form-control postinput py-2 px-3 text-start' />
                    {/* </form> */}
                    <button onClick={addComment} type="button" className={`${comment.length <= 0 && 'd-none'}  full-primary rounded-pill py-1 px-4 mt-2 border-0 outline-none`}>Post </button>
               </div>
          </div>
     )
}
