import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FollowApiCall } from '../../../../API/Assets';
import { isAuthenticate } from '../../../../API/Autho';
import { showAllpost } from '../../../../API/ShowAlldataPostandUsers';
import { employerImgurl, employerProfile, employerVideourl, postUrl, StudentImgurl, TIMI_OUT } from '../../../../variables';
import LikeUnlikeButton from '../../../AssetsComponents/LikeUnlikeButton';
import Alert from '../../util/Alert';
import Loader from '../../util/Loader';
import ListComment from './comment/ListComment';
import Createpost from './Createpost';
import SinglePostModel from './SinglePostModel';
import VideoFunctionc from './VideoFunctionC';



export default class Posts extends Component {
     constructor(props) {
          super(props);
          this.state = {
               data: [],

               following: false,
               loading: true,
               error: "",
               massage: "",

               alertType: ''
          }
     }

     setAllert = (massage, alertType) => {
          this.setState({ massage, alertType })
          setTimeout(() => {
               this.setState({ massage: '', alertType: '' })
          }, TIMI_OUT); // this value come form variable file, value is 3000 now
     }

     componentDidMount() {
          window.addEventListener('click', e => {
               if (e.target.classList.contains('mediaActivity')) {
                    // remove or add the class checked only from this element
                    // console.log('added class');
                    e.target.classList.toggle('postsActive-static');
               }
          });
          if (isAuthenticate().tokend) {
               showAllpost().then(responce => {
                    // console.log('responce -> ', responce);
                    if (responce.result) {
                         this.setState({ data: responce.result })
                    } else {
                         this.setState({ error: responce.error, alertType: 'error' })
                    }
                    this.setState({ loading: false })
               })
          }
     }
     followClick = (following_id, following_role, nameofuser) => {
          let token = isAuthenticate() && isAuthenticate().jwtoken;
          let data = {
               userId: this.props.userinfo.detailId,
               userRole: isAuthenticate() && isAuthenticate().role,
               followUnFollowId: following_id,
               followUnFollowRole: following_role
          };
          console.log(' TIMI_OUT ', TIMI_OUT);
          FollowApiCall(data, token).then(responce => {
               console.log('responce -> ', responce);
               if (responce.data && responce.data.success) {
                    // document.querySelectorAll(`.${following_id}_hide`).forEach((element)=>{
                    //      console.log('element -> ',element)
                    // });
                    Array.from(document.getElementsByClassName(`${following_id}_hide`)).forEach(element => {
                         // Now do something with my button
                         // console.log('element -> ',element)
                         // element.style.display = 'none'
                         element.innerHTML = "<a class='fw-bolder secund-heading p-1 primaryBGhover rounded'>connected<img width='20' class='Gif_purpul' src=" + `${process.env.PUBLIC_URL}/gif/CheckMark.gif` + " alt='GIF' /></a>"
                    });
                    this.setState({ following: !this.state.following, massage: `Your are now follwing ${nameofuser}`, alertType: 'success' })
                    setTimeout(() => {
                         this.setState({ massage: '' })
                    }, TIMI_OUT); // this value come form variable file, value is 3000 now
               } else {
                    console.log('error ', responce.error);
               }
          })

     }

     commentClick = (postId) => {
          let comment = document.getElementById(`comment-${postId}`);
          comment.classList.toggle('d-none');
     }



     // this will show all post like (artical and all media )
     randerPost = () => {
          // console.log(); 
          const { data, following } = this.state;
          return (
               data.filter(i => i.description).map((item, ind) =>
                    <div key={`post-${ind}`} className="">
                         <div className="bg-white  eachSection">
                              <div className="border-bottom mb-1 py-2 mx-3 d-flex"><div className="ms-auto post_moreOption"><span className="fw-bolder">&#8285;</span></div></div>
                              <div className="feed">
                                   {/* poster info (who post this ) */}
                                   <div className="py-3 mx-3">

                                        <div className="d-flex align-items-center">
                                             {/* img div  */}
                                             <div className="me-3 imgSmallS overflow-hidden rounded-circle">
                                                  {item.postedBy.firstName ?
                                                       (
                                                            <Link to={`/profile/${item.postedBy.firstName}-${item.postedBy.lastName}-${item.postedBy.uniq}`} ><div className="me-3 imgSmallS  overflow-hidden rounded-circle">
                                                                 <img src={`${StudentImgurl}/profile/${item.postedBy.profileImg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                            </div></Link>
                                                       ) : (
                                                            <Link to={`/profile/${item.postedBy.orgName}-${item.postedBy.uniq}`} ><div className="me-3 imgSmallS  overflow-hidden rounded-circle">
                                                                 <img src={`${employerImgurl}/profile/${item.postedBy.orgLogo.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg rounded-circle" alt="profileimg" />
                                                            </div></Link>
                                                       )
                                                  }
                                             </div>
                                             {/* name and headline div  */}
                                             <div className="flex1">
                                                  <div className="overflow-hidden lineHight1">
                                                       <h5 className="f-14px text-capitalize ">
                                                            <Link to={item.role === 'student' ? `/profile/${item.postedBy.firstName}-${item.postedBy.lastName}-${item.postedBy.uniq}` : `/profile/${item.postedBy.orgName}-${item.postedBy.uniq}`} className="userName" >
                                                                 {item.postedBy.orgName ? item.postedBy.orgName : item.postedBy.userName}
                                                            </Link> •
                                                            <span className='sm-txt'>  {new Date(item.updatedAt).toDateString()}
                                                            </span>
                                                       </h5>
                                                       <Link to={item.role === 'student' ? `/profile/${item.postedBy.firstName}-${item.postedBy.lastName}-${item.postedBy.uniq}` : `/profile/${item.postedBy.orgName}-${item.postedBy.uniq}`} className='color-unset fsXS' ><p className="text-capitalize"> {item.postedBy.headline.slice(0, 85)}{item.postedBy.headline.length > 90 ? ('...') : ('')}</p>
                                                            <span className="fw-light fsXXS card-text">{item.postedBy.address.city} • {item.postedBy.address.state} • {item.postedBy.address.country}</span></Link>
                                                  </div>
                                             </div>
                                             {/**** connect div if not connected  (this will show connect button if not connected) ****/}
                                             {item.postedBy._id === this.props.userinfo.detailId ? <i className="editIcon fas fa-pen" ></i> : (
                                                  item.postedBy.connections.some(e => e.onId === this.props.userinfo.detailId) ? '' : (<div className={`${item.postedBy._id}_hide`}>
                                                       <Link className="fw-bolder secund-heading p-1 primaryBGhover rounded" onClick={() => this.followClick(item.postedBy._id, item.role, item.postedBy.userName || item.postedBy.orgName)}> {/* <--- hare is conditionaly argument */}
                                                            <span>
                                                                 &#10011; Connect
                                                            </span>
                                                       </Link>
                                                  </div>))
                                             }
                                        </div>
                                   </div>
                                   {/* discription div (about post ) */}
                                   <div className="mx-3 py-2">
                                        <p className=''> {item.description.slice(0, 200)}{item.description.length > 201 ?
                                             (<Link to={`post/${item._id}`}>...More</Link>) : ('')}</p>
                                   </div>
                                   {/* media div (img video or more ) */}
                                   <div className="">
                                        <div className="feedimgsize">
                                             {/* this is img  */}
                                             {/* this link go to post */}
                                             <Link data-bs-toggle="modal" data-bs-target={`#showPostModel-${ind}`} to={`post/${item._id}`} className='d-block'>  {/* continue in single post */}
                                                  {
                                                       item.postedMediaFile && item.postedMediaFile.photo && (
                                                            <div className="d-flex flex-wrap">
                                                                 <img src={`${item.role === 'student' ? StudentImgurl : employerImgurl}/posts/${item.postedMediaFile.photo.fileName}`} alt="postimg" className="img-fluid" />

                                                            </div>
                                                       )
                                                  }
                                                  {
                                                       item.postedMediaFile && item.postedMediaFile.video && (
                                                            <div className="">
                                                                 <VideoFunctionc video={item.postedMediaFile.video.fileName} />  {/* this becouse video auto play */}
                                                            </div>

                                                       )
                                                  }
                                                  {
                                                       item.postedMediaFile && item.postedMediaFile.doc && (<span className="mx-3 py-2"> {item.postedMediaFile.doc.fileName.split('.')[0]} click to show </span>)
                                                  }
                                             </Link>
                                        </div>
                                   </div>
                                   {/* like comment and share div (assat div ) */}
                                   <div className=" mx-3">
                                        {/* Social Detials */}
                                        <div className="border-bottom py-2">
                                             <span className="fsXS card-text fontOpacity1 secund-heading">
                                                  <Link className="normalFontColor">likes <span className={`likeLenghth-${item._id}`}> {item.likes.length}</span></Link>  <Link className="normalFontColor float-end">{item.comments.length} comments</Link>
                                             </span>
                                        </div>
                                        {/* Social activity  */}
                                        <div className="fontOpacity1 my-2">
                                             {/* <span className="mediaActivity px-2 hovgray postIconSize rounded cursor-pointer"><span className="me-2"><i class="fas thumbs fa-thumbs-up"></i></span>link</span> */}
                                             <LikeUnlikeButton postLikes={item.likes} authLikedPost={this.props.userinfo.likes} authInfo={{ detailsId: this.props.userinfo.detailId }} postIdandRole={{ role: item.role, detailsId: item._id }} />
                                             <span className="px-2 hovgray postIconSize rounded cursor-pointer mediaActivity " onClick={e => this.commentClick(item._id)} ><span className="me-2"><i class="fas fa-comment"></i></span>comment</span>
                                             <span className="px-2 hovgray postIconSize rounded cursor-pointer mediaActivity "><span className="me-2"><i class="fas fa-share"></i></span>share</span>
                                        </div>

                                        {/* post comment input */}
                                        <ListComment setAllert={this.setAllert} postId={item._id} postRole={item.role} userinfo={this.props.userinfo} />
                                   </div>
                              </div>
                         </div>
                         <div id={`showPostModel-${ind}`} className="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="creatPostModelLabel" aria-hidden="true" >
                              <SinglePostModel authLikedPost={this.props.userinfo.likes} authD_id={this.props.userinfo.detailId} followingOrNot={following} followClick={this.followClick} postData={{ _id: item._id, role: item.role, description: item.description, postedBy: item.postedBy, updatedAt: item.updatedAt, postedMediaFile: item.postedMediaFile, likes: item.likes, comments: item.comments }} />
                         </div>
                    </div >
               )
          )
     }



     render() {
          const { massage, loading, alertType } = this.state;
          return (
               <div className="">
                    {/* Create post  */}
                    <Createpost setAllert={this.setAllert} userinfo={this.props.userinfo} />

                    {/* this is divider */}
                    <div className="d-flex align-items-center">
                         <hr className="dropdown-divider flex1" />
                         <span className='mx-3 sm-txt'>Feed</span>
                         <hr className="flex1" />
                    </div>
                    {
                         loading ? (
                              <Loader />
                         ) : ''
                    }
                    {
                         massage ? (
                              // <div className="z999 alert sticky-center-position alert-success text-start fade show" role="alert">
                              //      <strong>Success!</strong> {massage}.
                              //      {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                              // </div>
                              <Alert type={alertType} message={massage} />
                         ) : ''
                    }
                    {/* <div className="alert  alert-success position-sticky top-0 text-start fade show" role="alert">
                         <strong>Success!</strong> success to post.

                    </div> */}

                    {/* root feed div (hare is all post) */}
                    <div className="">
                         {/* now this all feed (post) shaw hare */}
                         {/* single feed div child  ( sing post controll)*/}
                         {this.randerPost()}

                    </div>
               </div>
          )
     }
}
