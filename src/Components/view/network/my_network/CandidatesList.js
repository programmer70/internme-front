import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { isAuthenticate } from '../../../../API/Autho';
import { showAllCandidatesOrEmployer, showAllconnections } from '../../../../API/ShowAlldataPostandUsers';
import { giveDetailIdbyUserId } from '../../../../API/utilAPI';
import { StudentImgurl } from '../../../../variables';
import FollowandUnfollowButton from '../../../AssetsComponents/FollowandUnfollowButton';

export default class MyCandidatesList extends Component {
    constructor(params) {
        super(params)
        this.state = {
            users: [],

            loading: false,
            error: '',
            message: '',
            success: false,
        }
    }

    // component mount fvunction 
    componentDidMount() {
        // console.log('cncon -> ', this.props.pathInfo);
        this.setState({ loading: true })
        const jwtoken = isAuthenticate().jwtoken;
        let userId = this.props.userInfo && this.props.userInfo.userId;
        let userType = this.props.userInfo && this.props.userInfo.userType;
        let userDetailId;
        giveDetailIdbyUserId({ userId }).then(responce => {
            // here we find the detail id base on user id 
            // console.log('responce c find id -> ', responce);
            if (responce.error) {
                // console.log('error myConnecitons idfind -> ', responce.error);
                this.setState({ error: responce.error, loading: false })
            } else {
                userDetailId = responce.data && responce.data.userDetailId
            }
            // console.log('userId -> ', userDetailId);
            const data = {
                userId: userDetailId,
                dbRef: 'userDetail'
            }
            showAllconnections(jwtoken, userType, data).then(responce => {
                // console.log('responce -> ', responce);
                if (responce.error) {
                    this.setState({ error: responce.error, loading: false, success: false })
                    this.props.AddconnectionsLength && this.props.AddconnectionsLength(0)

                } else {
                    this.setState({ users: responce.user, loading: false, success: true })
                    this.props.AddconnectionsLength && this.props.AddconnectionsLength(responce.user.length)

                }
            })

        })

    }



    renderCandidates = () => {
        const { users } = this.state;
        let path = this.props && this.props.pathInfo && this.props.pathInfo.path
        // console.log('pathname -> ',path);
        return (
            // <div>{JSON.stringify(this.state.users) } </div> // UI tommorow 
            users.map(({ onId }, i) =>
                path !== '/connections/:userId' ?
                    // if true
                    (<>
                        <div className="row MyConnectionsDiv" key={`myconnections-${i}`}>
                            <div className="imgdiv  col-3">
                                <Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} >
                                    <div className="imgSmallS m-auto rounded-circle avtar-div overflow-hidden">
                                        <img src={`${StudentImgurl}/profile/${onId.profileImg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} alt={`myconnections-img-${i}`} className='rounded-circle w-100 h-100 cardAvtarImg' /></div>
                                </Link>
                            </div>

                            <div className="col-9">
                                <div className="myconnectionsSomeInfo overflow-hidden">
                                    <h5 className="f-14px text-capitalize "><Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} className="userName" >{onId.userName}</Link> • <span className='sm-txt'>{new Date(onId.createdAt).toDateString()}</span></h5>
                                    <Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} className='color-unset fsXS' ><p className="text-capitalize">{onId.headline.slice(0, 85)}{onId.headline.length > 90 ? ('...') : ('')}</p></Link>
                                    <span className="fw-light fsXXS card-text">{onId.address.city} • {onId.address.state} • {onId.address.country}</span>
                                </div>

                            </div>

                        </div>
                        <div className="d-flex">
                            <span className='mx-2 w-100 btn sm-btn login-btn button-outline'>Message</span>
                            <span className='mx-2 w-100 btn sm-btn full-primary'><FollowandUnfollowButton setmessage={this.props.setmessage} authConnections={this.props.authUserInfo.connections} authInfo={{detailsId:this.props.authUserInfo.authDetailId}} userDetailidAndRole={{detailsId:onId._id,role:onId.role}} /></span>

                        </div>
                        <hr />
                    </>)
                    : (<div className="row MyConnectionsDiv align-items-center" key={`myconnections-${i}`}>
            <div className="imgdiv  col-1">
                <Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} >
                    <div className="imgNormalS m-auto rounded-circle avtar-div overflow-hidden">
                        <img src={`${StudentImgurl}/profile/${onId.profileImg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} alt={`myconnections-img-${i}`} className='rounded-circle w-100 h-100 cardAvtarImg' /></div>
                </Link>
            </div>

            <div className="col-8">
                <div className="myconnectionsSomeInfo border-bottom overflow-hidden">
                    <h5 className="des-txt-size text-capitalize "><Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} className="userName" >{onId.userName}</Link> • <span className='sm-txt'>{new Date(onId.createdAt).toDateString()}</span></h5>
                    <Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} className='color-unset' ><p className="text-capitalize">{onId.headline.slice(0, 85)}{onId.headline.length > 90 ? ('...') : ('')}</p></Link>
                    <span className="fw-light card-text">{onId.address.city} • {onId.address.state} • {onId.address.country}</span>
                </div>

            </div>
            <div className="col-3">
                {
                    this.props.authUserInfo && this.props.authUserInfo.authDetailId === onId._id ? (
                        <Link to={`/profile/${onId.firstName}-${onId.lastName}-${onId.uniq}`} >
                            <span className='mx-2 w-100 btn sm-btn login-btn button-outline'>My profile → </span>
                        </Link>
                    ) : (
                        <div className="d-flex">
                            <span className='mx-2 w-100 btn sm-btn login-btn button-outline'>Message</span>
                            <span className='mx-2 w-100 btn sm-btn full-primary'>
                                <FollowandUnfollowButton setmessage={this.props.setmessage} authConnections={this.props.authUserInfo.connections} authInfo={{detailsId:this.props.authUserInfo.authDetailId}} userDetailidAndRole={{detailsId:onId._id,role:onId.role}} />
                                </span>
                        </div>
                    )
                }


            </div>
            {/* <hr /> */}
        </div>)

            )
        )
    }

    render() {
        const { loading, users } = this.state;
        return (


            <>

                {loading ? (
                    <div className='loading-h-8rem  d-flex'>
                        <div className=' m-auto'>

                            <div className="spinner-border text-danger" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    </div>
                ) : (
                    users.length > 0 ? (

                        <div className="pb-3" id="MyCandidateConnectionsRow">
                            <h2 className="border-bottom font-s1p3">connected Candidates</h2 >
                            {this.renderCandidates()}

                        </div>
                    ) : ''
                )}
            </>



        )
    }
}
