import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { isAuthenticate } from '../../../../API/Autho';
import { showAllCandidatesOrEmployer } from '../../../../API/ShowAlldataPostandUsers';
import { StudentImgurl } from '../../../../variables';
import ConnectForMorepeople from '../../../AssetsComponents/ConnectForMorepeople';

export default class CandidatesList extends Component {
    constructor(params) {
        super(params)
        this.state = {
            users: [],

            loading: false,
            error: '',
            message: '',
            success: false,
        }
    }

    setmessage = (message) => {
        this.props.setmessage(message)
        console.log('message send');
    }

    // component mount function 
    componentDidMount() {
        this.setState({ loading: true })
        const jwtoken = isAuthenticate().jwtoken;
        showAllCandidatesOrEmployer(jwtoken, 'candidates').then(responce => {
            // console.log('responce candidate -> ',responce);
            if (responce.error) {
                this.setState({ error: responce.error, loading: false, success: false })
            } else {
                this.setState({ users: responce.users, loading: false, success: true })
            }
        })

    }






    dismissCardMethod = (cardId) => e => {
        /**
         * thet will use to hide a card from list 
        */
        // console.log(cardId);
        // console.log(document.getElementById(cardId),' --- card');
        document.getElementById(cardId).style.display = 'none'
    }

    renderCandidates = (users) => {
        
        // console.log('users. => ',users);

        return (
            users.map((item, i) =>
                <div id={`candidateCard-${i}`} key={`candidateCard-${i}`} className=" col-12 col-sm-6 col-lg-4 col-md-4 col-xl-3">

                    <div className="card" style={{ maxWidth: "16rem", minWidth: "11rem" }}>
                        {/* CardDismiss icon */}
                        <button onClick={this.dismissCardMethod(`candidateCard-${i}`)} id={`candidateCardDismiss-${i}`} className="cardDismissBtn z555 rounded-circle p-2">
                            <i className="cardDismiss fas fa-times"></i>
                        </button>

                        <div className="CardCover">
                            {/* img cover */}
                            <img src={`${StudentImgurl}/profile/${item.profileCoverimg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultcover.jpg`} className=" img-fluid card-img-top CardCoverImg obj-fit-cover" alt={`${item.userName}`} />
                            {/* avtar */}
                            <Link to={`/profile/${item.firstName}-${item.lastName}-${item.uniq}`} >
                                <div className="cardAvtar imgLS avtar-div">
                                    <img src={`${StudentImgurl}/profile/${item.profileImg.fileName}`} onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className="img-fluid cardAvtarImg" alt={`${item.userName}`} />
                                </div>
                            </Link>
                        </div>
                        <div className="card-body pt-5 text-center">
                            {/* text area */}
                            <Link to={`/profile/${item.firstName}-${item.lastName}-${item.uniq}`} className="text-dark">
                                <h5 className="card-title">{item.userName.slice(0, 15)}{item.userName.length > 15 ? (' ...') : ('')}</h5>
                                <p className="card-text fsXS fw-light mb-2">{item.headline.slice(0, 90)}{item.headline.length > 90 ? (' ...') : ('')}</p>
                            </Link>
                            <ConnectForMorepeople setmessage={this.setmessage} authconnections={item.connections} authdetailId={this.props.authUserDetails.detailId} userDetailidAndRole={{ role: item.user.role, userDetailsId: item._id }} /> 
                        </div>
                    </div>

                </div>


            )
        )



    }

    render() {
        let { loading,users } = this.state;

        {/**************** Need to Hide connected user from the list ************/}
        users = users.filter(element => element.user._id !== isAuthenticate().tokend) // filter the array of Candidates data (remove authenticate data (user))
        var findedIndex=[];
        users.map((item, i) => { item.connections.map((item2, i2) => { if (item2.onId === this.props.authUserDetails.detailId) { findedIndex.push(i) } }) }) // this formul will find already connected users index from the list 
        // now remove fromt the array 
        for (let index = findedIndex.length-1; index >= 0; index--) {
            users.splice(findedIndex[index],1)
        }
        // console.log('us -> ',users);

        return (
            <>
                <h2 className="border-bottom">All candidates</h2 >
                {loading ? (
                    <div className='loading-h-8rem  d-flex'>
                        <div className=' m-auto'>

                            <div className="spinner-border text-danger" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>


                        <div className="row g-3" id="candidateCardrow">

                            {users.length > 0 ? this.renderCandidates(users): ( <span>No more candidate for connect</span>)}

                        </div>
                    </div >
                )}
            </>


        )
    }
}
