import React, { Component } from 'react'
import { isAuthenticate } from '../../../../API/Autho'
import MyCandidatesList from '../my_network/CandidatesList'
import MyEmployerList from '../my_network/EmployerList'

export default class ManageMyNatwork extends Component {
    
    
    render() {
        // console.log('prop -> ',this.props.propsInfo);
        return (
            <div>
                {/* show all candidates  */}
                <div id="candidates">
                    {/* connected candidates */}
                    <MyCandidatesList setmessage={this.props.setmessage} AddconnectionsLength={this.props.AddconnectionsLength} authUserInfo={this.props.authUserInfo} userInfo={{ userId: isAuthenticate().tokend, userType: isAuthenticate().role }} /* not only authenticated user */ />

                </div>

                {/* show all employers  */}
                <div id="employers">
                    {/* conected employers*/}
                    <MyEmployerList setmessage={this.props.setmessage} AddconnectionsLength={this.props.AddconnectionsLength} authUserInfo={this.props.authUserInfo} userInfo={{ userId: isAuthenticate().tokend, userType: isAuthenticate().role }} /* not only authenticated user */ />

                </div>
            </div>
        )
    }
}
