import React from 'react';

const Loader = () => {
     return (
          <div className="spinner-border position-sticky d-block m-auto top-0 text-danger" role="status">
               <span className="visually-hidden">Loading...</span>
          </div>
     );
}

export default Loader;
