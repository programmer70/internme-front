import React from 'react';

const Alert = ({ type, message }) => {
     return (
          <div className={`z999 alert sticky-center-position alert-${type} text-start fade show`} role="alert">
               <strong>{type}!</strong> {message}.
               {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
          </div>
     );
}

export default Alert;
