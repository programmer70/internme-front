import React, { Component } from 'react';
import { useLocation } from 'react-router-dom';
import '../style/404page.css';

class NotFound404 extends Component {
    constructor(params) {
        super(params)
        document.title = '404 - not found'
        console.log('refer -> ', this.props.propsData);
    }
    render() {
        return (
            // this.props.usernotfound ? (
            //     <div class="mainbox">
            //     <div class="err">4</div>
            //     <i class="far fa-question-circle fa-spin"></i>
            //     <div class="err2">4</div>
            //     <div class="msg">Maybe this user was moved? Got deleted? Is hiding out in quarantine? Never existed in the first place?<p>Let's go <a href="/feed">Feed</a> - or - <button className='btn '>Back</button> and try from there.</p></div>
            // </div>
            // ) : (
            //     <div class="mainbox">
            //         <div class="err">4</div>
            //         <i class="far fa-question-circle fa-spin"></i>
            //         <div class="err2">4</div>
            //         <div class="msg">Maybe this page moved? Got deleted? Is hiding out in quarantine? Never existed in the first place?<p>Let's go <a href="#">home</a> and try from there.</p></div>
            //     </div>
            // )
            <div className="fullbody">
                <div class="mainbox-404">
                    <div class="err">4</div>
                    <i class="far fa-question-circle fa-spin"></i>
                    <div class="err2">4</div>
                    <div class="msg">Maybe this {this.props.propsData && this.props.propsData.usernotfount ? 'user' : 'page'} was moved? Got deleted? Is hiding out in quarantine? Never existed in the first place?<p>Let's go <a href="/feed">Feed</a> - or - <button onClick={() => this.props.propsData ? this.props.propsData.props.history.goBack() : this.props.history.goBack()} className='btn full-primary'>Back</button> and try from there.</p></div>
                </div>
            </div>
        );
    }
}

export default NotFound404;
