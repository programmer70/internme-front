/**
* all post show
*/

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthenticate } from '../../API/Autho';
import { checkEmployer, checkUser, showSingluser, showSinglemployer } from '../../API/ProfileAndPost';
import { userVerifyandGiveData } from '../../utilFunctions/GiveDatatoNavigation';
import Myinfos from '../view/feed/Myinfos';
import NewsLater from '../view/feed/NewsLater';
import Posts from '../view/feed/postCRUD/Posts';
import Navigation from './Navigation';
import '../../style/feed.css';

class Feed extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            error: '',
            redirectHome: false,
            data: {},
            imgpic: ''
        }
        document.title = 'Feed | InternMe'
    }
    componentDidMount() {
        this.setState({ loading: true })
        if (isAuthenticate()) {
            userVerifyandGiveData().then(responce => {
                // console.log('responce --> ', responce);
                if (responce.error === true) {
                    this.setState({ loading: responce.loading, redirectHome: responce.redirectHome })
                }
                this.setState({ loading: responce.loading, data: responce.data, imgpic: responce.imgpic })
            }).catch(error => {
                console.log('error -> ', error);
            });
            // console.log('info --> ',userVerifyandGiveData());
        }

    }
    render() {
        const { loading, redirectHome, data, imgpic } = this.state;
        if (redirectHome) {
            return <Redirect to={'/'} />
        }
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <header>

                        {isAuthenticate() && isAuthenticate().role === 'student' ? (
                            <Navigation fname={data.firstName} lname={data.lastName} unid={data.uniq} profileimg={imgpic} logdin={true} />
                        ) : (<Navigation orgName={data.orgName} unid={data.uniq} profileimg={imgpic} logdin={true} />)}
                    </header>
                    <div className='content-body-margin'>
                        <section><div className="adds m-4 p-4"></div></section>
                        <section className="container" id="gridContainer">
                            <aside className="gridItems">
                                <NewsLater />
                            </aside>
                            <main className="gridItems">
                                {isAuthenticate() && isAuthenticate().role === 'student' ?
                                    (<Posts userinfo={{ userName: data.userName, fname: data.firstName, lname: data.lastName, uniq: data.uniq, detailId: data._id, profileimg: imgpic,likes:data.likedPost }} />) :
                                    (<Posts userinfo={{ orgName: data.orgName, uniq: data.uniq, detailId: data._id, profileimg: imgpic,likes:data.likedPost }} />)}

                            </main>
                            <aside className="gridItems">
                                {isAuthenticate() && isAuthenticate().role === 'student' ? <Myinfos userInfo={{ fname: data.firstName, lname: data.lastName, uniq: data.uniq, detailId: data._id }} /> : <Myinfos userInfo={{ orgName: data.orgName, uniq: data.uniq, detailId: data._id }} />}

                            </aside>
                        </section>
                    </div>
                </div>
            )
        );
    }
}

export default Feed;
