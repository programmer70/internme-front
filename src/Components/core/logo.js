// import './navigation.css';
import '../../utilities.css'
const Logo = () => (
    <div className='footerlogo-box'>

        <a href="" className='a-deco-non d-flex align-items-center'>
            {/* <div id="InternMelogdiv" className="overflow-hidden rounded-circle">
                <img className="footerlogo w-100 h-100" src={process.env.PUBLIC_URL + '/logo192.png'} />
            </div> */}
            <span className='logoName'>Intern<b className='symbol'>Me</b></span>
        </a>

    </div>
)

export default Logo;