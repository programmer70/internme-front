import React, { Component } from 'react'

export default class Search extends Component {
    render() {
        return (
            <div>
                <h5>Your search - {this.props} - did not match any documents.</h5>

                <h5><b>Suggestions:</b></h5>
                <ul>
                    <li>Make sure that all words are spelled correctly.</li>
                    <li>Try different keywords.</li>
                    <li>Try more general keywords.</li>
                </ul>

            </div>
        )
    }
}
