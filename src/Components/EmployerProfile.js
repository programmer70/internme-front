import React, { Component } from 'react';
import { isAuthenticate } from '../API/Autho';
import { checkEmployer, showSinglemployer, showSinglEmployerbyName } from '../API/ProfileAndPost';
import Navigation from './core/Navigation';
import NotFound404 from './NotFound404';

// css file 
import '../utilities.css';
import './view/profile/profile.css';
import DashboardAndAbout from './view/employerProfile/DashboardAndAbout';
import Recommend from './view/employerProfile/Recommend';
import Contect from './view/employerProfile/Contect';
import Intro from './view/employerProfile/Intro';
import { userVerifyandGiveData } from '../utilFunctions/GiveDatatoNavigation';
import { TIMI_OUT } from '../variables';

export class EmployerProfile extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            error: '',
            massage: '',
            notFound: false,
            logdin: false,

            imgpic: '',
            data: {},
            email: '',
            OrgName: this.props.match.params.orgname,

            // authenticated info 
            headData: {},
            headImgpic: ''

        }
        document.title = `${this.props.match.params.orgname} | Profile`
    }

    setmassage = (massage) => {  // when any massage equire this function will fire 
        this.setState({ massage: massage })

        setTimeout(() => {
            this.setState({ massage: '' })
        }, TIMI_OUT);
    }

    componentDidMount() {
        this.setState({ loading: true })
        // console.log('eml profile ', isAuthenticate().tokend);
        if (isAuthenticate()) {
            // let id = isAuthenticate().tokend;
            // let token = isAuthenticate().token;
            // checkEmployer(id, token).then(responce => {
            //     // console.log('reponce for isAuth -> ', responce)
            //     if (responce.error) {
            //         // console.log('erro -> ', responce.error);
            //         this.setState({ logdin: false })
            //         return false
            //     }
            //     else if (responce.success || responce.detail) {
            //         // console.log('inof ',parsinfo );
            //         // console.log('respoce -> ', responce);
            //         this.setState({ logdin: true })
            //         showSinglemployer(id).then(responce => {
            //             console.log('singl user responce ', responce);
            //             this.setState({ headData: responce.user, headImgpic: responce.user.orgLogo.fileName })
            //             console.log('head data -> ', responce.user);
            //         })
            //         // return responce;
            //     }
            // })
            userVerifyandGiveData().then(responce => {
                // console.log('responce --> ', responce);
                if (responce.error === true) {
                    // console.log('eror on verifydata');
                    this.setState({ logdin: false })
                }
                this.setState({ headData: responce.data, headImgpic: responce.imgpic, logdin: true })
            }).catch(error => {
                console.log('error -> ', error);
            });
        }

        let data = {
            orgname: this.state.OrgName,
            uid: this.props.match.params.uid
        }
        this.showUserData(data)
        // showSinglEmployerbyName(data).then(responce => {
        //     // console.log('responce pro -> ',responce);
        //     if (responce.error) {
        //         this.setState({ notFound: true, loading: false })
        //     } else {
        //         // console.log('responce -> ', responce.user);
        //         this.setState({ data: responce.user, imgpic: responce.user.orgLogo.fileName, email: responce.user.user.email, loading: false })
        //         // document.title = `${responce.user.firstName} ${responce.user.lastName} | Profile`
        //     }
        // })
    }
    componentWillReceiveProps(props) {
        /**updata whe props update (url updata) */
        // console.log('componentWillReceiveProps');
        let data = {
            orgname: props.match.params.orgname,
            uid: props.match.params.uid
        }
        this.showUserData(data)
    }

    componentDidUpdate(prevProps) {
        // console.log('componentDidUpdate');
        if (!(this.props.match.params.orgname, prevProps.match.params.orgname)) {
            let data = {
                orgname: this.props.match.params.orgname,
                uid: this.props.match.params.uid
            }
            this.showUserData(data)
        }
    }

    showUserData = (data) => {  // acording to props change
        showSinglEmployerbyName(data).then(responce => {
            // console.log('data pro -> ', data);
            // console.log('responce pro -> ', responce);
            if (responce.error) {
                this.setState({ notFound: true, loading: false })
            } else {
                // console.log('responce -> ', responce.user);
                this.setState({ data: responce.user, imgpic: responce.user.orgLogo.fileName, email: responce.user.user.email, loading: false })
                // document.title = `${responce.user.firstName} ${responce.user.lastName} | Profile`
            }
        })
    }

    render() {
        const { notFound, headData, headImgpic, loading, logdin, email, data, imgpic, OrgName, error, massage } = this.state;
        if (notFound) {
            return <NotFound404 usernotfound={true} />
        }
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <header>
                        {
                            // give data to header(navBAR) according to user role (employer/candidate)                         
                            isAuthenticate() && isAuthenticate().role === 'employer' ?
                                (<Navigation orgName={headData.orgName} unid={headData.uniq} profileimg={headImgpic} logdin={logdin} />) : (
                                    <Navigation fname={headData.firstName} lname={headData.lastName} unid={headData.uniq} profileimg={headImgpic} logdin={logdin} />
                                )
                        }
                    </header>
                    <div className='content-body-padding'>
                        <section className='container'>

                            <div className="row g-4 pb-3">
                                <div className="col-md-9">
                                    {
                                        error ? (<div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>) : ''
                                    }
                                    {
                                        massage ? (
                                            <div className="alert sticky-center-position alert-success alert-dismissible text-start fade show z999" role="alert">
                                                <strong>Success!</strong> {massage}.
                                                {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                            </div>
                                        ) : ''
                                    }
                                    <div id='intro-sections' className='eachSection'>
                                        {/* this is intro sections img coverimg headline join connections  */}
                                        {/* <Intro setmassage={this.setmassage} unqid={data.uniq} uid={data.user && data.user._id} userintro={{ imgpic: imgpic, imgpicpath: data.profileImg && data.profileImg.Path, coverimg: profileCover, coverimgpath: data.profileCoverimg && data.profileCoverimg.Path, name: data.userName, docname: data.cvResume && data.cvResume.fileName, docpath: data.cvResume && data.cvResume.Path, fname: data.firstName, lname: data.lastName, gander: data.gander, headline: data.headline, network: data.connections, join: data.createdAt }} /> */}
                                        <Intro setmassage={this.setmassage} unqid={data.uniq} uid={data.user && data.user._id} userDetailidAndRole={{ role: data.user && data.user.role, userDetailsId: data._id }} userintro={{
                                            avtar: imgpic, avtarPath: data.orgLogo && data.orgLogo.Path, coverimg: data.orgCoverImag && data.orgCoverImag.fileName, coverimgpath: data.orgCoverImag && data.orgCoverImag.Path, name: data.orgName, docname: data.otherDocument && data.otherDocument.fileName, docpath: data.otherDocument && data.otherDocument.Path, headline: data.headline, docTitle: data.otherDocument && data.otherDocument.title, address: data.address,
                                            aboutDoc: data.otherDocument && data.otherDocument.somthingAbout, connections: data.connections, join: data.createdAt
                                        }} />
                                    </div>

                                    {/*  Activity dashboard and about */}
                                    <div id='eduNsk-section' className='eachSection'>
                                        <DashboardAndAbout setmassage={this.setmassage} userinfo={{ userid: data.user && data.user._id ,about:data.about}} />
                                    </div>



                                    <div id='eduNsk-section' className='eachSection'>
                                        {/* hare is all Education details   */}
                                        {/* <EducationsNskills setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} education={data.educations} skills={data.skills} uid={data.user && data.user._id} /> */}
                                    </div>

                                    <div id='contect-section' className='eachSection'>
                                        {/* hare is all contect details   */}
                                        <Contect setmassage={this.setmassage} uid={data.user && data.user._id} userContect={{ telphone: data.telphone, phone: data.phone, email: email, address: data.address, social: data.social }} />
                                        {/* <Contect setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} uid={data.user && data.user._id} userContect={{ phone: data.phone, address: data.address, social: data.social, email: email }} /> */}
                                    </div>

                                    {/* <div id='Documents-section' className='eachSection'>
                                        <div className='bg-white pb-3 p-3'>
                                            <h1 className='secund-heading text-capitalize fs-4'>Resume</h1>
                                            <div className='overflow-hidden'>
                                                 <Document file={data.cvResume &&  `${docurl}/${data.cvResume.fileName}`}></Document>
                                                </div>
                                            {data.cvResume && data.cvResume.fileName ?
                                                (<a target="_blanck" href={data.cvResume && `${docurl}/${data.cvResume.fileName}`}> Show My Resume</a>) : (
                                                    <div>No Document upload by <b>{data.userName}</b></div>
                                                )
                                            }
                                            {data.workSample && data.workSample ? (
                                                <>
                                                    <h1 className='m-0 mt-2 secund-heading text-capitalize fs-4 border-top'>Hare is my work sample</h1>
                                                    <small>(Go thare show my work)</small>
                                                    <br />
                                                    <a target="_blanck" href={data.workSample && `${data.workSample}`}> Go to My Work</a>
                                                </>
                                            )
                                                : ''}

                                        </div>
                                    </div> */}

                                </div>
                                <div className="col-md-3">
                                    <Recommend />
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            )
        )
    }
}

export default EmployerProfile
