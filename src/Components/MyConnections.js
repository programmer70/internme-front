import React, { Component } from 'react'
import Navigation from './core/Navigation';
import { userVerifyandGiveData } from '../utilFunctions/GiveDatatoNavigation';
import { isAuthenticate } from '../API/Autho';
import MyCandidatesList from './view/network/my_network/CandidatesList';
import MyEmployerList from './view/network/my_network/EmployerList';
import '../utilities.css'
import { TIMI_OUT } from '../variables';
class MyConnections extends Component {
    // continue tommorow 
    constructor(params) {
        super(params)
        this.state = {
            error: '',
            message: '',
            success: false,
            loading: false,

            redirectHome: false,
            logdin: false,
            data: {},
            imgpic: ''
        }

        // set title 
        document.title = `${this.props.location && this.props.location.state ? this.props.location.state.userName : 'User'}'s | Connections`
    }


    componentDidMount() {
        // console.log('param id ',this.props.location && this.props.location.state);
        this.setState({ loading: true })
        // console.log('component did mount');
        if (isAuthenticate()) {
            userVerifyandGiveData().then(responce => {
                // console.log('responce --> ', responce);
                if (responce.error === true) {
                    // console.log('eror on verifydata');
                    this.setState({ loading: responce.loading, error: responce.error, logdin: false })
                }
                this.setState({ loading: responce.loading, data: responce.data, imgpic: responce.imgpic, logdin: true })
            }).catch(error => {
                console.log('error -> ', error);
            });
        }
        this.setState({ loading: false, logdin: false })
    }

    setMessage = message => {
        this.setState({ message })
        setTimeout(() => {
            this.setState({ message: '' })
        }, TIMI_OUT); // this value come form variable file, value is 3000 now
    }

    render() {
        const { data, loading, logdin, imgpic, message } = this.state;
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <>
                    <header>
                        <Navigation fname={data.firstName} lname={data.lastName} unid={data.uniq} profileimg={imgpic} orgName={data.orgName} logdin={logdin} />
                    </header>
                    <div className='content-body-padding'>
                        <section className="container">
                            {/* <h1>this is MyConnections</h1>   here we write some content like total */}
                            {
                                message ? (
                                    <div className="z999 alert sticky-center-position alert-success text-start fade show" role="alert">
                                        <strong>Success!</strong> {message}.
                                        {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                    </div>
                                ) : ''
                            }
                            <div className="row">

                                <div className="col-md-9">
                                    <div className="cardsContainer bg-white eachSection p-3">

                                        {/* show all candidates  */}
                                        <div id="candidates">
                                            {/* connected candidates */}
                                            <MyCandidatesList setmessage={this.setMessage} pathInfo={this.props.match} authUserInfo={{ authDetailId: data._id, connections: data.connections }} userInfo={{ userId: this.props.match && this.props.match.params.userId, userType: this.props.location && this.props.location.state.userType }} /* not only authenticated user */ />

                                        </div>

                                        {/* show all employers  */}
                                        <div id="employers">
                                            {/* conected employers*/}
                                            <MyEmployerList setmessage={this.setMessage} pathInfo={this.props.match} authUserInfo={{ authDetailId: data._id, connections: data.connections }} userInfo={{ userId: this.props.match && this.props.match.params.userId, userType: this.props.location && this.props.location.state.userType }} /* not only authenticated user */ />

                                        </div>


                                    </div>
                                </div>



                            </div>
                        </section>
                    </div>
                </>
            )
        );
    }
}

export default MyConnections

