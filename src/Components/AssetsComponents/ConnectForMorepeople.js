import React, { useEffect, useState } from 'react'
import { FollowApiCall } from '../../API/Assets'
import { isAuthenticate } from '../../API/Autho';
import { checkFollowingOrNot, checkFollowingOrNotNewWay } from '../../utilFunctions/AssetsUtil'

// this function for multiuser render page like morePeople list page only connect(follow)
function ConnectForMorepeople(props) {
          // Declare a new state variable, which we'll call "connected"
          const [userDetailId,setUserDetailId] = useState('');
          const [connected,setConnected] = useState(false)
        //   const [connections,setConnections] = useState(props.authUserDetails.connections)

          console.log('auth authUserDetails => ',props.authUserDetails)

          useEffect(() => {
                    // console.log('useEffect 1');
                    checkFollowingOrNotNewWay(props.authconnections,props.authdetailId).then(responce => {
                              setUserDetailId(responce.userDetailId)
                              setConnected(responce.match);
                    })
          },[])

          const connectThis = () => {
                    console.log(' this is following')
                    console.log('fdl -- ',props);
                    let token = isAuthenticate() && isAuthenticate().jwtoken;
                    let data = {
                              userId: props.authdetailId,       
                              userRole: isAuthenticate() && isAuthenticate().role,
                              followUnFollowId: props.userDetailidAndRole && props.userDetailidAndRole.userDetailsId,
                              followUnFollowRole: props.userDetailidAndRole && props.userDetailidAndRole.role
                    }
                    console.log('-- ',data);
                    // this functions use to connect who is not connected
                    FollowApiCall(data, token).then(responce => {
                              // console.log('responce -> ', responce);
                              if (responce.data && responce.data.success) {
                                  setConnected(true)
                                  props.setmessage(responce.data.message)
                              } else {
                                  console.log('error ', responce.error);
                              }
                    })
                }
            
                return (
                    <div className='cursor-pointer mt-3'>
                        {
                            connected === false ? (
                                <span onClick={connectThis} className="btn rounded-pill outline-primary d-block"> <span>Connect</span></span>
                            ) : (
                                <span className="btn rounded-pill outline-primary d-block">connected<img width='20' className='Gif_purpul' src={`${process.env.PUBLIC_URL}/gif/CheckMark.gif`} alt="GIF" /></span>
                            )
                        }
                    </div>
                )
}


export default ConnectForMorepeople;
