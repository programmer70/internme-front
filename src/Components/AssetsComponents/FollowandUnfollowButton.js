import React, { useEffect, useState } from 'react'
import { FollowApiCall, unFollowApiCall } from '../../API/Assets'
import { isAuthenticate } from '../../API/Autho';
import {  checkFollowingOrNotNewWay } from '../../utilFunctions/AssetsUtil'

export default function FollowandUnfollowButton(props) {
     // Declare a new state variable, which we'll call "connected"
     const [connected, setConnected] = useState(false)
     const [connections,setConnections] = useState([])
     // console.log('connect => ',connections)

     useEffect(() => {
          // console.log('useEffect 1 , ',props.authConnections);
          // setConnections(props.authConnections)
          checkFollowingOrNotNewWay(props.authConnections,props.userDetailidAndRole.detailsId).then(responce => {
               setConnected(responce.match);
          })
     })

     const connectThis = () => {
          console.log(' this is following')
          console.log('fdl -- ', props);
          let token = isAuthenticate() && isAuthenticate().jwtoken;
          let data = {
               userId: props.authInfo.detailsId,
               userRole: isAuthenticate() && isAuthenticate().role,
               followUnFollowId: props.userDetailidAndRole && props.userDetailidAndRole.detailsId,
               followUnFollowRole: props.userDetailidAndRole && props.userDetailidAndRole.role
          }
          console.log('-- ', data);
          // this functions use to connect who is not connected
          FollowApiCall(data, token).then(responce => {
               // console.log('responce -> ', responce);
               if (responce.data && responce.data.success) {
                    props.authConnections.unshift({onId:props.userDetailidAndRole.detailsId})
                    setConnected(true)
                    props.setmessage(responce.data.message);
               } else {
                    console.log('error ', responce.error);
               }
          })
     }
     const disConnect = () => {
          console.log(' this is unfollowing')
          console.log('fdl -- ', props);
          let token = isAuthenticate() && isAuthenticate().jwtoken;
          let data = {
               userId: props.authInfo.detailsId,
               userRole: isAuthenticate() && isAuthenticate().role,
               followUnFollowId: props.userDetailidAndRole && props.userDetailidAndRole.detailsId,
               followUnFollowRole: props.userDetailidAndRole && props.userDetailidAndRole.role
          }
          console.log('-- ', data);
          // this functions use to connect who is not connected
          unFollowApiCall(data, token).then(responce => {
               // console.log('responce -> ', responce);
               if (responce.data && responce.data.success) {
                    props.authConnections.map((item,ind) => item.onId === props.userDetailidAndRole.detailsId ? props.authConnections.splice(ind,1) : '');
                    setConnected(false)
                    props.setmessage(responce.data.message);
               } else {
                    console.log('error ', responce.error);
               }
          })
     }

     return (
          <div className='cursor-pointer'>
               {
                    connected === false ? (
                         <span onClick={connectThis} className=''>Connect</span>
                    ) : (
                         <span onClick={disConnect}>Disconnect</span>
                    )
               }
          </div>
     )
}
