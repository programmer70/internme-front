import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { likeandUnlike } from '../../API/Assets';
import { isAuthenticate } from '../../API/Autho';
import { checkLikedorNot } from '../../utilFunctions/AssetsUtil';

export default function LikeUnlikeButton(props) {
     // Declare a new state variable, which we'll call "liked"
     const [liked, setLiked] = useState(false)
     const [likePost, setLikedPost] = useState([]) // this data list of like post by auth
     const [postLikes, setPostLikes] = useState([]) // this data list of like in post
     // console.log('connect => ',likePost)

     useEffect(() => {
          // console.log('useEffect 1 , ', props.authLikedPost);
          setLikedPost(props.authLikedPost)
          setPostLikes(props.postLikes)
          checkLikedorNot(likePost, props.postIdandRole.detailsId).then(responce => {
               setLiked(responce.match);

          })
     })
     useEffect(() => {
          Array.from(document.getElementsByClassName(`likeLenghth-${props.postIdandRole.detailsId}`)).forEach(element => {
               element.innerText = postLikes.length;
          })
     },[postLikes.length])

     const like_unlike = () => {
          console.log(' this is likeand unlike')
          console.log('liunprops -- ', props);
          let token = isAuthenticate() && isAuthenticate().jwtoken;
          let data = {
               authuserId: props.authInfo.detailsId,
               authuserRole: isAuthenticate() && isAuthenticate().role,
               postId: props.postIdandRole && props.postIdandRole.detailsId,
               postRole: props.postIdandRole && props.postIdandRole.role
          }
          // console.log('-- ', data);
          // let element_array = document.getElementsByClassName(`thumbsSpam-${props.postIdandRole.detailsId}`)

          // this functions use to like or remove like
          likeandUnlike(data, token).then(responce => {
               // console.log('responce -> ', responce);
               if (responce.data && responce.data.success) {
                    if (responce.data.like) {
                         props.authLikedPost.unshift({ posts: props.postIdandRole.detailsId })
                         props.postLikes.unshift({ user: props.authInfo.detailsId })
                         
                         Array.from(document.getElementsByClassName(`thumbsSpam-${props.postIdandRole.detailsId}`)).forEach(element => {
                              element.classList.add('postsActiveanimation')
                              // element.classList.contains
                         })
                         setLiked(true)
                    } if (responce.data.dislike) {
                         console.log('unlike');
                         props.authLikedPost.map((item, ind) => item.posts === props.postIdandRole.detailsId ? props.authLikedPost.splice(ind, 1) : '')
                         props.postLikes.map((item, ind) => item.user === props.authInfo.detailsId ? props.postLikes.splice(ind, 1) : '')
                         Array.from(document.getElementsByClassName(`thumbsSpam-${props.postIdandRole.detailsId}`)).forEach(element => {
                              element.classList.remove('postsActiveanimation')
                         })
                         setLiked(false)
                    }

               } else {
                    console.log('error ', responce.error);
               }
          })
     }

     return (
          <>
               {/* <span onClick={like_unlike} className={`px-2 hovgray postIconSize rounded cursor-pointer thumbsSpam-${props.postIdandRole.detailsId}`}><span className="me-2"><i class="fas thumbs fa-thumbs-up"></i></span>link</span> */}
               {
                    liked === false ? (
                         <span onClick={like_unlike} className={`px-2 hovgray postIconSize rounded cursor-pointer thumbsSpam-${props.postIdandRole.detailsId}`} ><span className="me-2"><i class="fas thumbs fa-thumbs-up"></i></span>link</span>
                    ) : (
                         <span onClick={like_unlike} className={`postsActive-static px-2 hovgray postIconSize rounded cursor-pointer thumbsSpam-${props.postIdandRole.detailsId}`}><span className="me-2"><i class="fas thumbs fa-thumbs-up"></i></span>link</span>
                    )
               }
          </>
     )
}