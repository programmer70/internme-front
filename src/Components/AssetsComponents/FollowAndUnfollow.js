import React, { Component } from 'react'
import { FollowApiCall, unFollowApiCall } from '../../API/Assets'

function FollowAndUnfollow(props) {           // this functions is only for single profile page 
    // console.log('TorF => ', props.following);
    const followClick = () => {
        props.ButtonClick(FollowApiCall)
    }

    const unfollowClick = () => {
        
        props.ButtonClick(unFollowApiCall)
    }

    return (
        <div className='d-inline-block cursor-pointer'>
            {
                props.following === false ? (
                    <a onClick={followClick} className=''>Follow</a>
                ) : (
                    <a onClick={unfollowClick} className=''>UnFollow</a>
                )
            }
        </div>
    )

}


export default FollowAndUnfollow;