import React, { Component, useEffect } from 'react';
import { checkUser, showSingluser, showSingluserbyName } from '../API/ProfileAndPost';
import Navigation from './core/Navigation';
import NotFound404 from './NotFound404';
import Intro from './view/profile/Intro';
import Recommend from './view/profile/Recommend';

import Contect from './view/profile/Contect';
import EducationsNskills from './view/profile/EducationsNskills';
import WorkExpss from './view/profile/WorkExpss';
import { StudentDocurl, TIMI_OUT } from '../variables';
import { Document, Page } from 'react-pdf';
// import Dashboard from './view/profile/DashboardAndAbout';
import { isAuthenticate } from '../API/Autho';
import querystring from 'querystring';
// css file 
import '../utilities.css';
import './view/profile/profile.css'
import { userVerifyandGiveData } from '../utilFunctions/GiveDatatoNavigation';
import DashboardAndAbout from './view/profile/DashboardAndAbout';


class Profile extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            logdin: false,
            usernotfount: false,
            data: {},
            headData: {},
            headImgpic: '',
            imgpic: '',
            profileCover: '',

            error: '',
            massage: ''
        }
    }

    setmassage = (massage) => {
        this.setState({ massage: massage })

        setTimeout(() => {
            this.setState({ massage: '' })
        }, TIMI_OUT);
    }

    componentDidMount() {
        this.setState({ loading: true })
        // console.log('eml profile ', isAuthenticate().tokend);
        if (isAuthenticate()) {
            // Chack the user is right or wrong 
            userVerifyandGiveData().then(responce => {
                // console.log('responce --> ', responce);
                if (responce.error === true) {
                    // console.log('eror on verifydata');
                    this.setState({ logdin: false })
                }
                this.setState({ headData: responce.data, headImgpic: responce.imgpic, logdin: true })
            }).catch(error => {
                console.log('error -> ', error);
            });
        }


        let data = {
            fname: this.props.match.params.fname,
            lname: this.props.match.params.lname,
            uid: this.props.match.params.uid
        }

        this.showUserData(data)
    }

    showUserData = (data) => {  // acording to props change
        showSingluserbyName(data).then(responce => {

            // console.log('responce pro -> ',responce);
            if (responce.error) {
                this.setState({ usernotfount: true, loading: false })
            } else {
                this.setState({ data: responce.user, profileCover: responce.user.profileCoverimg.fileName, imgpic: responce.user.profileImg.fileName, email: responce.user.user.email, loading: false })
                document.title = `${responce.user.firstName} ${responce.user.lastName} | Profile`
            }
        })
    }

    render() {
        const { error, massage, loading, usernotfount, email, data, imgpic, logdin, profileCover, headData, headImgpic } = this.state;
        if (usernotfount) {
            return <NotFound404 propsData={{ props: this.props, usernotfount}} />
        }
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <header>
                        {
                            // give data to header(navBAR) according to user role (employer/candidate)                         
                            isAuthenticate() && isAuthenticate().role === 'employer' ?
                                (<Navigation orgName={headData.orgName} unid={headData.uniq} profileimg={headImgpic} logdin={logdin} />) : (
                                    <Navigation fname={headData.firstName} lname={headData.lastName} unid={headData.uniq} profileimg={headImgpic} logdin={logdin} />
                                )
                        }
                    </header>
                    <div className='content-body-padding'>
                        <section className='container'>

                            <div className="row g-4 pb-3">
                                <div className="col-md-9">
                                    {
                                        error ? (<div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>) : ''
                                    }
                                    {
                                        massage ? (
                                            <div className="alert sticky-center-position alert-success alert-dismissible text-start fade show z999" role="alert">
                                                <strong>Success!</strong> {massage}.
                                                {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                            </div>
                                        ) : ''
                                    }
                                    <div id='intro-sections' className='eachSection'>
                                        {/* this is intro sections img coverimg about join connections  */}
                                        <Intro setmassage={this.setmassage} unqid={data.uniq} userDetailidAndRole={{ role: data.user && data.user.role, userDetailsId: data._id }} uid={data.user && data.user._id} userintro={{ imgpic: imgpic, imgpicpath: data.profileImg && data.profileImg.Path, coverimg: profileCover, coverimgpath: data.profileCoverimg && data.profileCoverimg.Path, name: data.userName, docname: data.cvResume && data.cvResume.fileName, docpath: data.cvResume && data.cvResume.Path, fname: data.firstName, lname: data.lastName, gander: data.gander, headline: data.headline, connections: data.connections, join: data.createdAt, languages: data.languages }} />
                                    </div>

                                    <div id='eduNsk-section' className='eachSection'>
                                        <DashboardAndAbout setmassage={this.setmassage} userinfo={{ userid: data.user && data.user._id, about: data.about }} />
                                    </div>



                                    <div id='eduNsk-section' className='eachSection'>
                                        {/* hare is all Education details   */}
                                        <EducationsNskills setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} education={data.educations} skills={data.skills} uid={data.user && data.user._id} />
                                    </div>
                                    <div id='work-section' className='eachSection'>
                                        {/* hare is all Education details   */}
                                        <WorkExpss setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} work={data.workrole} uid={data.user && data.user._id} />
                                    </div>

                                    <div id='contect-section' className='eachSection'>
                                        {/* hare is all contect details   */}
                                        <Contect setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} uid={data.user && data.user._id} userContect={{ phone: data.phone, address: data.address, social: data.social, email: email }} />
                                    </div>
                                    <div id='contect-section' className='eachSection'>
                                        <div className='bg-white pb-3 p-3'>
                                            <h1 className='secund-heading text-capitalize fs-4'>Resume</h1>
                                            {/* <div className='overflow-hidden'>
                                                 <Document file={data.cvResume &&  `${StudentDocurl}/profile/${data.cvResume.fileName}`}></Document>
                                                </div> */}
                                            {data.cvResume && data.cvResume.fileName ?
                                                (<a target="_blanck" href={data.cvResume && `${StudentDocurl}/profile/${data.cvResume.fileName}`}> Show My Resume</a>) : (
                                                    <div>No Document upload by <b>{data.userName}</b></div>
                                                )
                                            }
                                            {data.workSample && data.workSample ? (
                                                <>
                                                    <h1 className='m-0 mt-2 secund-heading text-capitalize fs-4 border-top'>Hare is my work sample</h1>
                                                    <small>(Go thare show my work)</small>
                                                    <br />
                                                    <a target="_blanck" href={data.workSample && `${data.workSample}`}> Go to My Work</a>
                                                </>
                                            )
                                                : ''}

                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <Recommend />
                                </div>

                            </div>
                        </section>

                    </div>
                    {/* <div className="colorChang"><h1>nwe</h1></div> */}
                </div>

            )

        );
    }
}

export default Profile;
