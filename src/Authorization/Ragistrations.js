import React, { Component } from 'react';
import DocViewer from 'react-doc-viewer';
import Footer from '../Components/core/Footer';
import Logo from '../Components/core/logo';
import '../utilities.css';
import { checkUser } from '../API/ProfileAndPost'
import { fileUploadAPI } from '../API/Upload';
import { creat_profileOrpost } from '../API/ProfileAndPost';
import NotFound404 from '../Components/NotFound404';
import { Link, Redirect } from 'react-router-dom';
import querystring from 'querystring';
import { StudentImgurl, employerImgurl, Backendurl, employerDocurl } from '../variables';
// // import './index_jquery';
// let StudentImgurl = `${Backendurl}/img/canditateImg/profile`;
class Ragistrations extends Component {
    constructor(params) {
        super(params)
        this.state = {
            error: '',
            loading: false,
            rediract: false,
            notfound: false,

            pfileName: '',
            pfilePath: '',
            cfilneName: '',
            cfilePath: '',
            docfileName: '',
            docfilePath: '',
            orglogname: '',
            orgPath: '',

            // only for candidate
            firstName: '',
            lastName: '',
            userName: '',
            languages: ['English'],
            gander: 'Mr.',
            city: '',
            github: '',
            workSample: '',

            // only for org 
            orgName: '',
            telphone: '',
            aboutDoc: '',
            docTitle: '',
            website: '', // only for org 

            // bothe 
            phone: '',
            state: '',
            country: '',
            youtube: '',
            instagram: '',
            facebook: '',
            linkedin: '',
            twitter: '',

            about: '',
            headline: '',

            userType: this.props.match.params.userType
        }

        document.title = `${this.state.userType} | Ragistration`;
    }



    handalPhotoClick = name => e => {
        if (name === 'avtar') {
            document.getElementById('ProfileimgUpload').click()
            // console.log('profile click ->',e.target.files );
        }
        else if (name === 'cover') {
            document.getElementById('coverimgUpload').click()
            // console.log('cover click ->',e.target.files);
        }
        else if (name === 'orgLogo') {
            document.getElementById('orgLogoUpload').click()
            // console.log('orgLogo click ->',e.target.files);
        }
    }

    handleCandidateFileChange = name => photo => {
        this.setState({ error: '', loading: true })
        var formData = new FormData();
        let id = this.props.match.params.id
        let token = this.props.match.params.token

        formData.append(name, photo.target.files[0])
        console.log('formData -> ', formData);
        // console.log('file -> ', photo.target.files[0]);

        if (name === 'avtar' || name === 'cover') {

            fileUploadAPI('userProfile', 'new', 'imgupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else {
                    // console.log('data is file -> ', data);
                    if (name === 'avtar') {
                        this.setState({ pfileName: data.fileName, pfilePath: data.filePath, error: '', loading: false });
                    } else {
                        this.setState({ cfilneName: data.fileName, cfilePath: data.filePath, error: '', loading: false });
                    }
                }
            })
        } else if (name === 'doc') {
            fileUploadAPI('userProfile', 'new', 'docupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else this.setState({ docfileName: data.fileName, docfilePath: data.filePath, error: '', loading: false });
            })
        }

    };
    handleEmployerFileChange = name => photo => {
        this.setState({ error: '', loading: true })
        var formData = new FormData();
        let id = this.props.match.params.id
        let token = this.props.match.params.token

        formData.append(name, photo.target.files[0])
        console.log('formData -> ', formData);
        // console.log('file -> ', photo.target.files[0]);

        if (name === 'orgLogo' || name === 'cover') {

            fileUploadAPI('employerProfile', 'new', 'imgupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else {
                    // console.log('data is file -> ', data);
                    if (name === 'orgLogo') {
                        this.setState({ orglogname: data.fileName, orgPath: data.filePath, error: '', loading: false });
                    } else {
                        this.setState({ cfilneName: data.fileName, cfilePath: data.filePath, error: '', loading: false });
                    }
                }
            })
        } else if (name === 'otherOrgVerificationDocument') {
            fileUploadAPI('employerProfile', 'new', 'docupload', id, token, name, 'profile', formData).then(data => {
                // console.log('upload reponce -> ', data);

                if (data.error) this.setState({ error: data.error, loading: false })

                else this.setState({ docfileName: data.fileName, docfilePath: data.filePath, error: '', loading: false });
            })
        }

    };

    isValid = () => {
        const { about, headline, firstName, userType, orgName, lastName, userName, phone, telphone, languages, city, state, country } = this.state
        this.setState({ error: '', loading: true })
        if (userType === 'student') {
            if ((firstName.length && lastName.length && userName.length) === 0) {
                this.setState({ error: 'Organization Name is required', loading: false })
                return false;
            }
            if ((firstName.length && lastName.length && userName.length) <= 3) {
                this.setState({ error: 'Organization Name must be 3 or more charecters', loading: false })
                return false;
            }
            if ((firstName.includes('-') || lastName.includes('-')) === true) {
                this.setState({ error: 'Organization Name does not accept "-". pleas dont use', loading: false })
                return false;
            }
            if ((phone.length || telphone.length) === 0) {
                this.setState({ error: 'Phone is required', loading: false })
                return false
            }
            if (phone.length > 10 || phone.length < 10) {
                this.setState({ error: 'Phone must have 10 charecters only ', loading: false })
                return false
            }
            if ((city.length && state.length && country.length) === 0) {
                this.setState({ error: 'Address all fields are required', loading: false })
                return false
            }
            if ((city.length && state.length && country.length) <= 1) {
                this.setState({ error: 'Pleas fill valide Address fields', loading: false })
                return false
            }
            if (languages.length === 0) {
                this.setState({ error: 'languages fiels are required', loading: false })
                return false
            }
            if (headline.length === 0) {
                this.setState({ error: 'headline is required ', loading: false })
                return false;
            }
            if (headline.length > 100) {
                this.setState({ error: 'headline is must have under 100 charecters ', loading: false })
                return false
            }
        }
        if (userType === 'employer') {
            if (orgName.length === 0) {
                this.setState({ error: 'Name is required', loading: false })
                return false;
            }
            if (orgName.length <= 3) {
                this.setState({ error: 'Name must be 3 or more charecters', loading: false })
                return false;
            }
            if (orgName.includes('-') === true) {
                this.setState({ error: 'FirstName and last name does not accept "-". pleas dont use', loading: false })
                return false;
            }
            if (phone.length === 0) {
                this.setState({ error: 'Phone is required', loading: false })
                return false
            }
            if (phone.length > 10 || phone.length < 10) {
                this.setState({ error: 'Phone must have 10 charecters only ', loading: false })
                return false
            }
            if ((city.length && state.length && country.length) === 0) {
                this.setState({ error: 'Address all fields are required', loading: false })
                return false
            }
            if ((city.length && state.length && country.length) <= 1) {
                this.setState({ error: 'Pleas fill valide Address fields', loading: false })
                return false
            }
            if (headline.length === 0) {
                this.setState({ error: 'headline is required ', loading: false })
                return false;
            }
            if (headline.length > 100) {
                this.setState({ error: 'headline is must have under 100 charecters ', loading: false })
                return false
            }
        }

        return true
    }

    handleinputValueChange = name => event => {
        // lang
        const { languages } = this.state;
        // console.log('key -> ', name, ' value  -> ', event.target.value);
        // for languages 
        if (name === 'language') {
            var lang = languages;
            if (!lang.includes(event.target.value)) {
                lang.unshift(event.target.value)
                this.setState({ languages: lang })
            } else if (lang.includes(event.target.value)) {
                let indx = lang.indexOf(event.target.value)
                if (indx > -1) {
                    lang.splice(indx, 1);
                    this.setState({ languages: lang })
                }
            }
        } else {

            this.setState({ [name]: event.target.value })
        }
    }

    clickSubmit = (e) => {
        const { about, headline, orgName, docTitle, website, aboutDoc, telphone, userType, firstName, lastName, userName, phone, gander, languages, city, state, country, pfileName, pfilePath, cfilneName, cfilePath, workSample, docfileName, docfilePath, orgPath, orglogname, youtube, instagram, facebook, github, twitter, linkedin } = this.state
        e.preventDefault()
        let id = this.props.match.params.id
        let token = this.props.match.params.token

        this.setState({ error: '', loading: true })
        let data = {};
        if (this.isValid()) {
            if (userType === 'student') {
                data.profileImg = {
                    fileName: pfileName,
                    Path: pfilePath
                }
                data.profileCoverimg = {
                    fileName: cfilneName,
                    Path: cfilePath
                }
                data.cvResume = {
                    fileName: docfileName,
                    Path: docfilePath
                }

                data.firstName = firstName;
                data.lastName = lastName;
                data.userName = userName;
                data.gander = gander;
                data.languages = languages;
                data.workSample = workSample
            }
            if (userType === 'employer') {

                data.orgLogo = {
                    fileName: orglogname,
                    Path: orgPath
                }
                data.orgCoverImag = {
                    fileName: cfilneName,
                    Path: cfilePath
                }
                data.otherDocument = {
                    fileName: docfileName,
                    Path: docfilePath,
                    title: docTitle,
                    somthingAbout: aboutDoc
                }
                data.telphone = telphone
                data.website = website
                data.orgName = orgName
            }


            data.phone = phone;

            data.about = about;
            data.headline = headline
            data.address = { city, state, country }
            data.social = { youtube, facebook, instagram, github, twitter, linkedin }
            if (userType === 'student') {
                creat_profileOrpost('userprofile', id, token, data).then(responce => {
                    // console.log('responce -> ', responce)
                    if (responce) {
                        this.setState({ rediract: true, error: '', loading: false });

                    }
                    else this.setState({ error: responce.error, loading: false })

                })
            } else if (userType === 'employer') {
                creat_profileOrpost('employerProfile', id, token, data).then(responce => {
                    // console.log('responce -> ', responce)
                    if (responce) {
                        this.setState({ rediract: true, error: '', loading: false });

                    }
                    else this.setState({ error: responce.error, loading: false })

                })
            }

        }
        // this.setState({ error: massag })
        setTimeout(() => {
            this.setState({ error: '' })
        }, 3000);

    }

    componentDidMount() {
        // this.setState({ languages: ['English'], gander: 'male' })
        document.getElementById('btnSubmit').setAttribute('disabled', 'true')
        // check user 
        let id = this.props.match.params.id
        let token = this.props.match.params.token
        const { location: { search } } = this.props;
        if (search) {
            // console.log('query massg -> ', search);
            let propmsg = querystring.parse(search);
            let massag;
            for (const key in propmsg) {
                massag = propmsg[key]
            };
            // console.log('query massg -> ', massag);
            this.setState({ error: massag })
            setTimeout(() => {
                this.setState({ error: '' })
            }, 3000);
        }

        checkUser(id, token).then(responce => {
            // console.log('responce.data.detail => ', responce)
            if (responce.error) {
                this.setState({ notfound: true })
            }
            else if (responce.detail === true) {
                this.setState({ rediract: true, userType: responce.role })
            }
            else if (responce.role !== this.state.userType) {
                this.setState({ notfound: true })
            }

        })
    }

    orgVerification = areayId => e => {
        // v --. verification
        let vsl = document.getElementById('vSocial')
        let vdoc = document.getElementById('vDocument')
        let vws = document.getElementById('vWebsite')
        if (areayId === 'vDocument') {
            vdoc.style.display = 'block';
            vsl.style.display = 'none';
            vws.style.display = 'none';
        }
        if (areayId === 'vSocial') {
            vsl.style.display = 'block';
            vdoc.style.display = 'none';
            vws.style.display = 'none';
        }
        if (areayId === 'vWebsite') {
            vws.style.display = 'block';
            vdoc.style.display = 'none';
            vsl.style.display = 'none';
        }

    }

    confirmCheckBox = e => {
        if (!e.target.checked) {
            document.getElementById('btnSubmit').setAttribute('disabled', 'true')
        }
        else {
            document.getElementById('btnSubmit').removeAttribute('disabled')
        }
    }

    render() {
        const { notfound, error, loading, rediract, userType, orglogname, cfilneName, pfileName } = this.state
        if (rediract) {
            return <Redirect to={{ pathname: '/login', state: { userType: userType } }} />
        }
        if (notfound) {
            return <NotFound404 usernotfound={true} />
        }
        return (
            <div>

                <div className='mainpage bg-color d-flex text-center bg-white hight-non width-non'>

                    <div className='mx-auto w-50'>
                        <div className='authrizeArea minmax-90100 px-0 d-inline-block py-3'>
                            {/* singup form  */}
                            <div className='my-3 text-start'>
                                <Logo />
                            </div>
                        </div>
                        <div id='singup' className='secundry-shadow authrizeArea minmax-90100'>
                            <div className='text-start mb-4 border-bottom'>
                                <h2 className='first-heading m-0'>Fill all the details</h2>
                                <p className='small-text my-1'>Show your profile to all company in the world
                                </p>
                            </div>
                            {
                                loading ? (
                                    <div className="spinner-border position-sticky top-0 text-danger" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                ) : ''
                            }
                            {
                                error ? (
                                    <div className="alert alert-danger position-sticky top-0 text-start fade show" role="alert">
                                        <strong>Error!</strong> {error}.
                                        {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                    </div>
                                ) : ''

                            }
                            <form encType="multipart/form-data" className="row g-3 text-start">
                                <div className="imgViwer">
                                    {userType === 'employer' ?
                                        (
                                            // <div className='orglogo rounded-circle d-flex m-auto'>
                                            //     {/* <div className='' > */}
                                            //     <img src={`${employerImgurl}/profile/${orglogname}`}
                                            //         onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} id="profileImg" className='profileImg' />

                                            //     <input onChange={this.handleEmployerFileChange('orgLogo')} type="file" hidden accept="image/*" name='orgLogo' id="orgLogoUpload" />

                                            //     <span onClick={this.handalPhotoClick('orgLogo')} className='ProfilCamra'><i className="fas fa-camera"></i></span>
                                            //     {/* </div> */}
                                            // </div>

                                            <div className=' overflow-hidden mb-3'>
                                                <div className='coverImg'>
                                                    <img src={`${employerImgurl}/profile/${cfilneName}`}
                                                        onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultcover.jpg`} className='cover' id="coverImg" />
                                                    <div className='orglogo rounded-circle d-flex'>
                                                        {/* <div className='' > */}
                                                        <img src={`${employerImgurl}/profile/${orglogname}`}
                                                            onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} id="profileImg" className='profileImg' />

                                                        <input onChange={this.handleEmployerFileChange('orgLogo')} type="file" hidden accept="image/*" name='orgLogo' id="orgLogoUpload" />

                                                        <span onClick={this.handalPhotoClick('orgLogo')} className='ProfilCamra'><i className="fas fa-camera"></i></span>
                                                        {/* </div> */}
                                                    </div>
                                                    <input onChange={this.handleEmployerFileChange('cover')} name='cover' type="file" hidden accept="image/*" id="coverimgUpload" />
                                                    <span onClick={this.handalPhotoClick('cover')} className='coverImgCamra'><i className="fas fa-camera"></i></span>
                                                </div>
                                            </div>
                                        )
                                        : (
                                            <div className=' overflow-hidden mb-3'>
                                                <div className='coverImg'>
                                                    <img src={`${StudentImgurl}/profile/${cfilneName}`}
                                                        onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultcover.jpg`} className='cover' id="coverImg" />
                                                    <div className='profile rounded-circle d-flex'>
                                                        {/* <div className='' > */}
                                                        <img src={`${StudentImgurl}/profile/${pfileName}`}
                                                            onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} id="profileImg" className='profileImg' />

                                                        <input onChange={this.handleCandidateFileChange('avtar')} type="file" hidden accept="image/*" name='avtar' id="ProfileimgUpload" />

                                                        <span onClick={this.handalPhotoClick('avtar')} className='ProfilCamra'><i className="fas fa-camera"></i></span>
                                                        {/* </div> */}
                                                    </div>
                                                    <input onChange={this.handleCandidateFileChange('cover')} name='cover' type="file" hidden accept="image/*" id="coverimgUpload" />
                                                    <span onClick={this.handalPhotoClick('cover')} className='coverImgCamra'><i className="fas fa-camera"></i></span>
                                                </div>
                                            </div>
                                        )
                                    }

                                </div>
                                <p className='border-bottom fw-bold font-s1p3 '>{userType === 'student' ? 'Personal Info' : 'Organization Info'}:</p>

                                {userType === 'employer' ? (<div className="col-12">
                                    <label htmlFor="orgName" className="form-label">Organization Name</label>
                                    <input type="url" onChange={this.handleinputValueChange('orgName')} className="form-control" id="orgName" name="orgName" placeholder="eg. Xyz pvt. ltd." />
                                </div>) : ''}


                                {
                                    userType === 'student' ? (
                                        <>
                                            <div className="col-md-2">
                                                <label for="inputState" className="form-label">Title</label>
                                                <select id="inputState" className="form-select" onChange={this.handleinputValueChange('gander')}>
                                                    <option selected value='Mr.'>Mr.</option>
                                                    <option value="Mrs.">Mrs.</option>
                                                    <option value="Ms.">Ms.</option>
                                                </select>
                                            </div>

                                            <div className="col-md-6">
                                                <label htmlFor="firstName" className="form-label required">First Name</label>
                                                <input required onChange={this.handleinputValueChange('firstName')} type="text" className="form-control" name='firstName' id="firstName" placeholder="eg. Rahul" />
                                            </div>
                                            <div className="col-md-4">
                                                <label htmlFor="lastName" className="form-label required">Last Name</label>
                                                <input required onChange={this.handleinputValueChange('lastName')} type="text" className="form-control" name='lastName' id="lastName" placeholder="eg. Sharma" />
                                            </div>

                                            <div className="col-6">
                                                <label htmlFor="userName" className="form-label required">User Name</label>
                                                <input required onChange={this.handleinputValueChange('userName')} type="text" className="form-control" name="userName" id="userName" placeholder="1234 Main St" />
                                            </div>
                                        </>
                                    ) : ''
                                }

                                <div className="col-6">
                                    <label htmlFor="phone" className="form-label required">Mobile no.</label>
                                    <input required onChange={this.handleinputValueChange('phone')} type="number" className="form-control" name="phone" id="phone" minLength="10" maxLength="10" placeholder="1234567890" />
                                </div>

                                {userType === 'employer' ? (
                                    <div className="col-6">
                                        <label htmlFor="telphone" className="form-label required">Tel no.</label>
                                        <input required onChange={this.handleinputValueChange('telphone')} type="number" className="form-control" name="telphone" id="telphone" minLength="15" maxLength="15" placeholder="022-4567890" />
                                    </div>
                                ) : ''}


                                {
                                    userType === 'student' ? (
                                        <>
                                            <div className="col-3">
                                                <p className='fw-bold required'>language know:</p>

                                                <div className="form-check">
                                                    <input onChange={this.handleinputValueChange('language')} className="form-check-input" type="checkbox" name="English" id="english" value="English" defaultChecked />
                                                    <label className="form-check-label" htmlFor="english">
                                                        English
                                                    </label>
                                                </div>
                                                <div className="form-check">
                                                    <input onChange={this.handleinputValueChange('language')} className="form-check-input" type="checkbox" name="Hindi" id="hindi" value="Hindi" />
                                                    <label className="form-check-label" htmlFor="hindi">
                                                        Hindi
                                                    </label>
                                                </div>
                                                <div className="form-check">
                                                    <input onChange={this.handleinputValueChange('language')} className="form-check-input" type="checkbox" name="Marathi" id="marathi" value="Marathi" />
                                                    <label className="form-check-label" htmlFor="marathi">
                                                        Marathi
                                                    </label>
                                                </div>
                                            </div>


                                        </>
                                    ) : ''
                                }
                                <div className={userType === 'student' ? "col-9" : "col-12"}>

                                    <label htmlFor="headline" className="form-label required" >HeadLine :</label>
                                    <input onChange={this.handleinputValueChange('headline')} type="text" className='form-control primary-input-style border-1' name='headline' placeholder='Mern Dev • Devops ..' id="headline" />

                                </div>




                                <p className='border-bottom fw-bold font-s1p3 '>Address:</p>
                                <div className="col-md-4">
                                    <label htmlFor="city" className="form-label required">City</label>
                                    <input required onChange={this.handleinputValueChange('city')} type="text" placeholder="eg. Mumbai" className="form-control" name='city' id="city" />
                                </div>
                                <div className="col-md-4">
                                    <label htmlFor="state" className="form-label required">State</label>
                                    <input required onChange={this.handleinputValueChange('state')} type="text" placeholder="eg. Maharashtra" className="form-control" name='state' id="state" />
                                </div>
                                <div className="col-md-4">
                                    <label htmlFor="country" className="form-label required">Country</label>
                                    <input required onChange={this.handleinputValueChange('country')} type="text" placeholder="eg. India" className="form-control" name='country' id="country" />
                                </div>




                                {userType === 'student' ?
                                    (
                                        <>
                                            <p className='border-bottom fw-bold font-s1p3 '>Social Network:</p>

                                            <div className="col-12">
                                                <label htmlFor="github" className="form-label">GitHub</label>
                                                <input type="url" onChange={this.handleinputValueChange('github')} className="form-control" id="github" name="github" placeholder="eg. https://github.com/" />
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="youtube" className="form-label">Youtube</label>
                                                <input type="url" onChange={this.handleinputValueChange('youtube')} className="form-control" id="youtube" name="youtube" placeholder="eg. https://Youtube.com/" />
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="twitter" className="form-label">Twitter</label>
                                                <input type="url" onChange={this.handleinputValueChange('twitter')} className="form-control" id="twitter" name="twitter" placeholder="eg. https://twitter.com/" />
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="linkedin" className="form-label">LinkedIn</label>
                                                <input type="url" onChange={this.handleinputValueChange('linkedin')} className="form-control" id="linkedin" name="linkedin" placeholder="eg. https://linkedin.com/" />
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="facebook" className="form-label">Facebook</label>
                                                <input type="url" onChange={this.handleinputValueChange('facebook')} className="form-control" id="facebook" name="facebook" placeholder="eg. https://Facebook.com/" />
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="instagram" className="form-label">Instagram</label>
                                                <input type="url" onChange={this.handleinputValueChange('instagram')} className="form-control" id="instagram" name="instagram" placeholder="eg. https://Instagram.com/" />
                                            </div>
                                            <p className='border-bottom fw-bold'>Description:</p>
                                            <div className="col-12">
                                                <label htmlFor="about" className="form-label required">About You</label>
                                                <textarea minLength="100" maxLength="500" onChange={this.handleinputValueChange('about')} type="text" className="form-control " id="about" name="about" placeholder="eg. Somthing about you ...." rows="5" required />
                                            </div>
                                        </>
                                    ) : ''
                                }

                                {userType === 'employer' ?
                                    (
                                        <>
                                            <p className='border-bottom fw-bold font-s1p3  required'>Organization verification:</p>
                                            <p className="small-text my-1">( Any on is require for verification your Organization. )</p>
                                            {/*  v-> verification and r-> radio */}
                                            <div className="form-check col-md-4">

                                                <input defaultChecked onChange={this.orgVerification('vWebsite')} className="form-check-input" id="rvWebsite" type="radio" name='verification' />
                                                <label className="form-check-label" htmlFor="rvWebsite">
                                                    By Website
                                                </label>
                                            </div>

                                            <div className="form-check col-md-4">

                                                <input onChange={this.orgVerification('vSocial')} className="form-check-input" id="rvSocial" type="radio" name='verification' /><label className="form-check-label" htmlFor="rvSocial">
                                                    By Social
                                                </label>
                                            </div>

                                            <div className="form-check col-md-4">

                                                <input onChange={this.orgVerification('vDocument')} className="form-check-input" id="rvDocument" type="radio" name='verification' /><label className="form-check-label" htmlFor="rvDocument">
                                                    By Document
                                                </label>
                                            </div>

                                            <div className='form-control'>

                                                {/* <p>By Website</p> */}
                                                <div style={{ display: 'block' }} id='vWebsite'>
                                                    <fieldset>
                                                        <legend>Website</legend>
                                                    </fieldset>
                                                    <div className="col-12">
                                                        <label htmlFor="website" className="form-label">Website url</label>
                                                        <input type="url" onChange={this.handleinputValueChange('website')} className="form-control" id="website" name="website" placeholder="eg. https://mywebsite.com/" />
                                                    </div>

                                                </div>


                                                {/* <p>By Social</p> */}
                                                <div style={{ display: 'none' }} id="vSocial">
                                                    <fieldset>
                                                        <legend>Social</legend>
                                                    </fieldset>
                                                    <p className="small-text my-1">( You can give any of those. )</p>
                                                    <div className="col-12">
                                                        <label htmlFor="linkedin" className="form-label">linkedin</label>
                                                        <input type="url" onChange={this.handleinputValueChange('linkedin')} className="form-control" id="linkedin" name="linkedin" placeholder="eg. https://linkedin.com/" />
                                                    </div>
                                                    <div className="col-12">
                                                        <label htmlFor="twitter" className="form-label">Twitter</label>
                                                        <input type="url" onChange={this.handleinputValueChange('twitter')} className="form-control" id="twitter" name="twitter" placeholder="eg. https://twitter.com/" />
                                                    </div>
                                                    <div className="col-12">
                                                        <label htmlFor="youtube" className="form-label">Youtube</label>
                                                        <input type="url" onChange={this.handleinputValueChange('youtube')} className="form-control" id="youtube" name="youtube" placeholder="eg. https://Youtube.com/" />
                                                    </div>
                                                    <div className="col-12">
                                                        <label htmlFor="facebook" className="form-label">Facebook</label>
                                                        <input type="url" onChange={this.handleinputValueChange('facebook')} className="form-control" id="facebook" name="facebook" placeholder="eg. https://Facebook.com/" />
                                                    </div>
                                                    <div className="col-12">
                                                        <label htmlFor="instagram" className="form-label">Instagram</label>
                                                        <input type="url" onChange={this.handleinputValueChange('instagram')} className="form-control" id="instagram" name="instagram" placeholder="eg. https://Instagram.com/" />
                                                    </div>
                                                </div>



                                                {/* <p>By Document</p> */}
                                                <div style={{ display: 'none' }} id="vDocument">
                                                    <fieldset>
                                                        <legend>Document</legend>
                                                    </fieldset>
                                                    <div className="col-12">
                                                        <label htmlFor="docTitle" className="form-label">Document Title</label>
                                                        <input type="text" onChange={this.handleinputValueChange('docTitle')} className="form-control" id="docTitle" name="docTitle" placeholder="eg. Text bill" />
                                                    </div>
                                                    <p className='border-bottom fw-bold'>Upload Document:</p>
                                                    <div className="input-group mb-3">
                                                        {/* <label className="input-group-text" htmlFor="resume">Upload</label> */}
                                                        <input onChange={this.handleEmployerFileChange('otherOrgVerificationDocument')} accept=".pdf, .doc, .docx, .odt" type="file" className="form-control" name='otherOrgVerificationDocument' id="vdoc" />
                                                    </div>

                                                    <div className="col-12">
                                                        <label htmlFor="about" className="form-label">About Document</label>
                                                        <textarea maxLength="200" onChange={this.handleinputValueChange('aboutDoc')} type="text" className="form-control " id="about" name="about" placeholder="eg. Somthing about Document ...." rows="5" required />
                                                    </div>
                                                </div>


                                            </div>

                                        </>
                                    ) : ''
                                }

                                {userType === 'student' ? (
                                    <>
                                        <p className='border-bottom fw-bold'>Upload CV/Resume:</p>
                                        <div className="input-group mb-3">
                                            {/* <label className="input-group-text" htmlFor="resume">Upload</label> */}
                                            <input onChange={this.handleCandidateFileChange('doc')} accept=".pdf, .doc, .docx, .odt" type="file" className="form-control" name='doc' id="resume" />
                                        </div>
                                        <div className="col-12">
                                            <label htmlFor="sample" className="form-label">Work Sample link</label>
                                            <input type="url" onChange={this.handleinputValueChange('workSample')} className="form-control" id="sample" name="sample" placeholder="eg. https://sample.com/" />
                                        </div>
                                    </>
                                ) : ''}

                                {userType === 'employer' ? (
                                    <>
                                        <p className='border-bottom fw-bold font-s1p3 '>About You Organization:</p>
                                        <div className="col-12">
                                            <label htmlFor="about" className="form-label required">Description</label>
                                            <textarea minLength="100" maxLength="500" onChange={this.handleinputValueChange('about')} type="text" className="form-control " id="about" name="about" placeholder="eg. Somthing about Organization ...." rows="5" required />
                                        </div>
                                    </>
                                ) : ''

                                }



                                <div className="col-12">
                                    <div className="form-check">
                                        <input required onChange={this.confirmCheckBox} className="form-check-input" type="checkbox" id="gridCheck" />
                                        <label className="form-check-label" htmlFor="gridCheck">
                                            Check me to submit
                                        </label>
                                    </div>
                                </div>

                                <div className="col-12 text-center">
                                    {/* <button type="submit" className="btn btn-primary">Sign in</button> */}
                                    <button type='submit' id='btnSubmit' onClick={this.clickSubmit} className='btn my-3 fw-bold authrizeArea-btn-p full-primary form-btn-width70 form-btn-radius' >Submit</button>
                                </div>
                            </form>
                            <h6 className='my-3'><span className='fw-normal'>Already on InternMe? </span><Link className='a-deco-non a-primary' to={'/login'}>Sing in</Link> ,<span className='fw-normal'> Back to </span><Link className='a-deco-non a-primary' to={'/'}>Home</Link></h6>
                        </div>
                    </div>
                </div>
                <div className='container'>
                    <footer>
                        <Footer />
                    </footer>
                </div>
            </div >
        );
    }
}

export default Ragistrations;
