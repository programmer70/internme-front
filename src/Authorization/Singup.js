import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { isAuthenticate, singup } from '../API/Autho';
import Logo from '../Components/core/logo';
// import hostName from '../verable.json'
import '../utilities.css';
import Footer from '../Components/core/Footer';
import { checkUser } from '../API/ProfileAndPost';
import NotFound404 from '../Components/NotFound404';
class Singup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: '',
            emailError: '',
            passwordError: '',
            loading: false,
            ragister: false,
            redirectfeed: false,
            redirectToken1: '',
            redirectToken2: ''
        }

        

        // set titel 
        document.title = 'Singup | internMe'
    }
    handalChange = name => event => {
        this.isValidEmail()
        this.isValidPassword()
        this.ButtonDisable()
        this.setState({
            error: '',
            loading: false,
            [name]: event.target.value

        })

        /// show password hide and show if any write in input box
        if(name==='password'){
            if(event.target.value === ''){
                document.getElementById('showHide').style.display = 'none'
            }else{
                document.getElementById('showHide').style.display = 'block'
            }
        }
        this.isValidEmail()
        this.isValidPassword()
        this.ButtonDisable()
    }

    isValidEmail = () => {
        const { email } = this.state;
        let isEmail = /^([0-9a-zA-Z]){2}([_0-9a-zA-Z]){1,30}?(\.[_0-9a-zA-Z])*@([a-zA-Z]){2,15}\.([a-zA-Z]){2,15}$/
        this.setState({ error: '', loading: false })
        if (email.length === 0) {
            this.setState({ emailError: 'Email is required !!' })
            return false;
        }
        if (!isEmail.test(email)) {
            this.setState({ emailError: 'Please give valid email !!' })
            return false;
        }
        else {
            this.setState({ emailError: '' })
            return true;
        }

        // return true;
    }
    isValidPassword = () => {
        const { password } = this.state;
        let isPassword = /^(?=.*[A-Za-z])(?=.*[\d!@#$%^&*<>?:";'_+])(?=.*[!@#$%^&*<>?:";'_+])[A-Za-z\d!@#$%^&*<>?:";'_+]{6,}$/;
        this.setState({ error: '', loading: false })

        if (password.length === 0) {
            this.setState({ passwordError: 'Password is required !!' })
            return false;
        }
        if (password.length <= 6) {
            this.setState({ passwordError: 'Password must be, more than 6 characters !!' })
            return false;
        }
        if (!isPassword.test(password)) {
            this.setState({ passwordError: 'Password must have special charecters' })
            return false;
        }
        else {
            this.setState({ passwordError: '' });
            return true;
        }
    }
    ButtonDisable = () => {
        let btn = document.getElementById('submitbtn');
        if (this.isValidEmail() === false || this.isValidPassword() === false) {
            btn.setAttribute('disabled', true);
        }
        else if (this.isValidEmail() === true && this.isValidPassword() === true) {
            btn.removeAttribute('disabled');
        }
        // console.log(this.isValidPassword());
        return;
    }
    SubmitClick = event => {
        event.preventDefault()
        this.setState({ loading: true })
        const { email, password } = this.state;
        if (this.isValidEmail() && this.isValidPassword()) {

            var data = {
                email,
                password
            };
            if (this.props.match.params.userType) {
                singup(data, this.props.match.params.userType).then(data => {
                    // console.log('error is -> ', data);
                    if (data.error) this.setState({ email: '', password: '', error: data.error, loading: false })
                    else this.setState({ email: '', password: '', error: '', passwordError: '', emailError: '', loading: false, ragister: true, redirectToken1: data.token, redirectToken2: data.id });
                });
            }else{
                this.setState({loading:false,error:'Not found userType'})
            }
        };
    };

    showHideClick = (e) => {
        let passwordElmt = document.getElementById('password');
        if (e.target.innerText === 'Show') {
            // e.target.innerText
            passwordElmt.setAttribute('type', 'text');
            e.target.innerText = 'Hide';
        } else if (e.target.innerText === 'Hide') {
            passwordElmt.setAttribute('type', 'password');
            e.target.innerText = 'Show';
        }
    }

    componentDidMount() {
        this.setState({ loading: true });
        // hide element (hide show password)
        document.getElementById('showHide').style.display = 'none';

        // disaple buttone
        document.getElementById('submitbtn').setAttribute('disabled','true')
        if (isAuthenticate()) {
            let id = isAuthenticate().tokend;
            let token = isAuthenticate().token;
            checkUser(id, token).then(responce => {
                // console.log('reponce for isAuth -> ',responce)
                if (responce.error) {
                    console.log('erro -> ', responce.error);
                    this.setState({ redirectfeed: false, loading: false })
                    return false
                }
                else if (responce.success || responce.detail) {
                    // console.log('inof ',parsinfo );
                    // console.log('respoce -> ', responce);
                    this.setState({ redirectfeed: true, loading: false })
                    // return responce;
                }
            })
        } else {
            this.setState({ error: '', loading: false })
        }

    }

    render() {
        const { redirectfeed, redirectToken1, redirectToken2, ragister, loading, email, password, error, emailError, passwordError } = this.state;
        if (ragister) {
            return <Redirect to={`/ragisteration/${this.props.match.params.userType}/${redirectToken2}-${redirectToken1}`} />
        }
        if (redirectfeed) {
            return <Redirect to={`/feed`} />
        }
        return (
            <div className=''>
                <div className='mainpage bg-color d-flex flex-column text-center'>

                    {this.props.match.params.userType && this.props.match.params.userType === 'student' || this.props.match.params.userType === 'employer' ?
                        (
                            <div className='mx-auto w-50rem '>
                                <div className='my-3'>
                                    <Logo />
                                </div>

                                <div className=' my-3 text-center'>
                                    <h1 className='first-heading first-heading-size'>Let's Join our professional cammunity!</h1>
                                    {
                                        this.props.match.params.userType === 'student' ? (
                                            <p className="secund-heading font-s1p3">I am student/employee. i want a internship/job</p>) : (
                                            <p className="secund-heading font-s1p3">I am employer. i want a candidate/employee</p>)
                                    }
                                </div>


                                {
                                    loading ? (
                                        <div class="spinner-border text-danger d-block m-auto" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    ) : ''
                                }
                                {
                                    error ? (
                                        <div class="alert alert-danger alert-dismissible text-start fade show authrizeArea m-auto mb-2 w-75 p-3" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''
                                }
                                <div id='singup' className='primary-shadow authrizeArea'>
                                    {/* singup form  */}
                                    <form>
                                        <div className='text-start '>

                                            <label className='small-text form-label fw-bold font-p9' htmlFor='email' >Email </label>
                                            <input onChange={this.handalChange('email')} name='email' value={email} className=' primary-input-style form-control' id='email' type='email' />
                                            {/* show error if error equired  */}
                                            {emailError ? (
                                                <small className='text-danger'>{emailError}</small>
                                            ) : ''}
                                        </div>
                                        <div className='text-start position-relative'>

                                            <label className='small-text form-label fw-bold font-p9' htmlFor='password' >password <small> (6 or more characters)</small></label>
                                            <input onChange={this.handalChange('password')} name='password' value={password} className=' primary-input-style form-control' id='password' type='password' />
                                            {passwordError ? (
                                                <small className='text-danger'>{passwordError}</small>
                                            ) : ''}
                                           <span onClick={this.showHideClick}  className='showHide' id='showHide'>Show</span>
                                        </div>
                                        <p className='smallfont small-text'>
                                            By clicking Agree & Join, you agree to the LinkedIn User <Link to={''} className='fw-bolt a-deco-non a-primary'>Agreement </Link>, <Link to={''} className='fw-bolt a-deco-non a-primary'>Privacy Policy</Link>, and <Link to={''} className='fw-bolt a-deco-non a-primary'>Cookie Policy</Link>.
                                        </p>
                                        <button onClick={this.SubmitClick} type='submit' id='submitbtn' className='btn fw-bold authrizeArea-btn-p full-primary form-btn-width100 form-btn-radius' >Agree & join</button>
                                        {
                                            this.props.match.params.userType && this.props.match.params.userType === 'student' ? (
                                                <>
                                                    <p className='divider my-3'>
                                                        or
                                            </p>
                                                    <button className='btn fw-bold authrizeArea-btn-p outline-primary form-btn-width100 form-btn-radius' >Join with social</button>
                                                </>
                                            ) : ''
                                        }


                                    </form>
                                    <h6 className='my-3'><span className='fw-normal'>Already on InternMe? </span><Link className='a-deco-non a-primary' to={{pathname:'/login',state:{userType:this.props.match.params.userType}}}>Sing in</Link> ,<span className='fw-normal'> Back to </span><Link className='a-deco-non a-primary' to={'/'}>Home</Link></h6>

                                </div>
                            </div>
                        )
                        : <NotFound404 />}  {/* if params not student or employer then not found */}
                </div>
                <div className='container'>
                    <footer className=''>
                        <Footer />
                    </footer>
                </div>
            </div>
        );
    }
}

export default Singup;
