import React from 'react';
import { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { isAuthenticate } from '../API/Autho';

// PrivetRouting.jsx or ProtectedRote.jsx  --> react-router-dom v5 👈
const PrivetRouting = ({ component: Component, ...rest }) => (
    // Props means components passed down to the pricate route component
    // await (console.log(' --> ',authorNot()))
    <Route {...rest} render={props =>
        isAuthenticate() ? (
            <Component {...props} />
        ) : (
            <Redirect to={
                {
                    pathname: '/login',
                    state: { from: props.location }
                }
            }
            />
        )
    } />
)




export default PrivetRouting;
