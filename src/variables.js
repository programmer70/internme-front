module.exports = ({
    APIurl: 'http://localhost:8080/api',
    Backendurl: 'http://localhost:8080',

    // files url 
    // student 
    StudentImgurl: 'http://localhost:8080/img/student', // profile or post
    StudentDocurl: 'http://localhost:8080/doc/student', // profile or post
    StudentVideourl: 'http://localhost:8080/video/student', // profile or post

    //employer
    employerImgurl: 'http://localhost:8080/img/employer', // profile or post
    employerDocurl: 'http://localhost:8080/doc/employer', // profile or post
    employerVideourl: 'http://localhost:8080/video/employer', // profile or post

    // users profile 
    candidateProfile: `http://localhost:8080/api/userProfile`,
    employerProfile: `http://localhost:8080/api/employerProfile`,

    //post
    postUrl: 'http://localhost:8080/api/post',

    // hare is alert timing
    TIMI_OUT: 3000
})