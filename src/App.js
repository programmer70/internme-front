// import logo from './logo.svg';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import AllRouter from './AllRouter';
// import './App.css';

const App = () => (
  <BrowserRouter>
      <AllRouter/>
  </BrowserRouter>
)

export default App;
