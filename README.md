# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)


# only for me .. Importent think i learn while creating this project

1. this method is very usefull for update nested obj in state: [this.setState(prevState => ({](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/Components/view/employerProfile/Contect.js#L57-L64)

2. auto hide the all message and error in 3 secund (please call this fuction in class method ): [setTimeout(() => {](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/Components/view/employerProfile/Contect.js#L112-L114)

3. take the obj from parent class and set as child state: [componentDidMount() {](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/Components/view/employerProfile/Contect.js#L23-L35)

4. set the page title in constructor: [document.title = 'Feed | InternMe'](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/Components/core/Feed.js#L21-L21)

5. multiple url for single page: [<Route exact path={['/','/home']} component={Home} />](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/AllRouter.js#L23-L23)

6. pass props to specific components: [<Route exact path='/login' render={(props)=> <Ragistration {...props}/>} component={Login} />](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/AllRouter.js#L27-L27)

7. render not foud page if not matching any rout : [<Route component={NotFound404} />](https://github.com/naseemkhan7021/InternMe_front/blob/f8b5e5bf95f395ba16029f52114d2eb781860b2d/src/AllRouter.js#L32-L32)

8. functons use to handle Axios requist to share multiple components [userChanckandGivedata](https://github.com/naseemkhan7021/InternMe_front/blob/dfefcd823ca421af36fe65a449adf33856a53796/src/Components/core/Feed.js#L27-L27)

9. Array filter : 
    -  a method to **filter** an array array according to uses. this is use to filter Array of object come from server to to so all candidates data not Authenticate data.[users = users.filter(element => element._id !== isAuthenticate().tokend)](https://github.com/naseemkhan7021/InternMe_front/blob/509b838f415dc0707bbd1a18e015bc66f051717b/src/Components/view/network/CandidatesList.js#L45-L45)

    - also this will filter data logically [${isAuthenticate() && isAuthenticate().tokend === item.user._id ? 'd-none' : ''}](https://github.com/naseemkhan7021/InternMe_front/blob/509b838f415dc0707bbd1a18e015bc66f051717b/src/Components/view/network/EmployerList.js#L50-L50)

10. Play video on on View port only other wice pause --> [const [isInViewport3, boxEl3] = useIsInViewport({](https://github.com/naseemkhan7021/InternMe_front/blob/d85cfa64263bf0404d1a10182abd53113e1e4b74/src/Components/view/feed/postCRUD/VideoFunctionC.js#L6-L21)

11. check the value in arrayOfObject and give true or false --> [item.postedBy.connections.some(e => e.onId === this.props.userinfo.detailId) ? '' : (<div className="">](https://github.com/naseemkhan7021/InternMe_front/blob/d85cfa64263bf0404d1a10182abd53113e1e4b74/src/Components/view/feed/postCRUD/Posts.js#L94-L100)

12. give conditionaly arguments in functions --> [item.role, item.postedBy.userName || item.postedBy.orgName)}> {/* <--- hare is conditionaly argument */}](https://github.com/naseemkhan7021/InternMe_front/blob/d85cfa64263bf0404d1a10182abd53113e1e4b74/src/Components/view/feed/postCRUD/Posts.js#L114)
